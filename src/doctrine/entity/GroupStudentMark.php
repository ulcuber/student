<?php



/**
 * GroupStudentMark
 */
class GroupStudentMark
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $value;

    /**
     * @var \GroupStudent
     */
    private $student;

    /**
     * @var \Subject
     */
    private $subject;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param boolean $value
     *
     * @return GroupStudentMark
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return boolean
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set student
     *
     * @param \GroupStudent $student
     *
     * @return GroupStudentMark
     */
    public function setStudent(\GroupStudent $student = null)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student
     *
     * @return \GroupStudent
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set subject
     *
     * @param \Subject $subject
     *
     * @return GroupStudentMark
     */
    public function setSubject(\Subject $subject = null)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return \Subject
     */
    public function getSubject()
    {
        return $this->subject;
    }
}

