<?php



/**
 * GroupStudentHobby
 */
class GroupStudentHobby
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \GroupStudent
     */
    private $student;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GroupStudentHobby
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set student
     *
     * @param \GroupStudent $student
     *
     * @return GroupStudentHobby
     */
    public function setStudent(\GroupStudent $student = null)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student
     *
     * @return \GroupStudent
     */
    public function getStudent()
    {
        return $this->student;
    }
}

