<?php
Route::group([
    'prefix' => 'mark',
    'as' => 'mark.',
        ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'GroupStudentMarkController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'GroupStudentMarkController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'GroupStudentMarkController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'GroupStudentMarkController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'GroupStudentMarkController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'GroupStudentMarkController@delete']);

});
