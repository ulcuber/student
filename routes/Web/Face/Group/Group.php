<?php
Route::group([
    'prefix' => 'group',
    'as' => 'group.',
        ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'GroupController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'GroupController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'GroupController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'GroupController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'GroupController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'GroupController@delete']);

});
