<?php
Route::group([
    'prefix' => 'hobby',
    'as' => 'hobby.',
        ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'GroupStudentHobbyController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'GroupStudentHobbyController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'GroupStudentHobbyController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'GroupStudentHobbyController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'GroupStudentHobbyController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'GroupStudentHobbyController@delete']);

});
