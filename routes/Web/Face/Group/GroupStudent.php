<?php
Route::group([
    'prefix' => 'student',
    'as' => 'student.',
        ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'GroupStudentController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'GroupStudentController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'GroupStudentController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'GroupStudentController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'GroupStudentController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'GroupStudentController@delete']);

});
