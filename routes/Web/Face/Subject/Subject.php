<?php
Route::group([
    'prefix' => 'subject',
    'as' => 'subject.',
        ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'SubjectController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'SubjectController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'SubjectController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'SubjectController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'SubjectController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'SubjectController@delete']);

});
