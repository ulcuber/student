<?php

Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    'namespace' => 'Web\Admin',
    'middleware' => 'auth',
], function () {

    Route::group([
        'prefix' => 'group',
        'as' => 'group.',
        'namespace' => 'Group',
    ], function () {
        Route::get('{groupId?}',            ['as' => 'index',  'uses' => 'GroupController@index'])->where('groupId', '[0-9]+');
        Route::get('create',                ['as' => 'create', 'uses' => 'GroupController@create']);
        Route::post('save',                 ['as' => 'save',   'uses' => 'GroupController@save']);
        Route::get('{groupId}/edit',        ['as' => 'edit',   'uses' => 'GroupController@edit'])->where('groupId', '[0-9]+');
        Route::put('{groupId}/update',      ['as' => 'update', 'uses' => 'GroupController@update'])->where('groupId', '[0-9]+');
        Route::delete('{groupId}/delete',   ['as' => 'delete', 'uses' => 'GroupController@delete'])->where('groupId', '[0-9]+');
        include 'Group/GroupStudent.php';
    });

    Route::group([
        'prefix' => 'subject',
        'as' => 'subject.',
        'namespace' => 'Subject',
    ], function () {
        Route::get('{id?}',                 ['as' => 'index',  'uses' => 'SubjectController@index'])->where('id', '[0-9]+');
        Route::get('create',                ['as' => 'create', 'uses' => 'SubjectController@create']);
        Route::post('save',                 ['as' => 'save',   'uses' => 'SubjectController@save']);
        Route::get('{id}/edit',             ['as' => 'edit',   'uses' => 'SubjectController@edit'])->where('id', '[0-9]+');
        Route::put('{id}/update',           ['as' => 'update', 'uses' => 'SubjectController@update'])->where('id', '[0-9]+');
        Route::delete('{id}/delete',        ['as' => 'delete', 'uses' => 'SubjectController@delete'])->where('id', '[0-9]+');
    });

    Route::group([
        'prefix' => 'teacher',
        'as' => 'teacher.',
        'namespace' => 'Teacher',
    ], function () {
        Route::get('{teacherId?}',          ['as' => 'index',  'uses' => 'TeacherController@index'])->where('teacherId', '[0-9]+');
        Route::get('create',                ['as' => 'create', 'uses' => 'TeacherController@create']);
        Route::post('save',                 ['as' => 'save',   'uses' => 'TeacherController@save']);
        Route::get('{teacherId}/edit',      ['as' => 'edit',   'uses' => 'TeacherController@edit'])->where('teacherId', '[0-9]+');
        Route::put('{teacherId}/update',    ['as' => 'update', 'uses' => 'TeacherController@update'])->where('teacherId', '[0-9]+');
        Route::delete('{teacherId}/delete', ['as' => 'delete', 'uses' => 'TeacherController@delete'])->where('teacherId', '[0-9]+');
    });

    Route::group([
        'prefix' => 'subjectTeacher',
        'as' => 'subjectTeacher.',
        'namespace' => 'Teacher',
    ], function () {
        Route::put('update',                ['as' => 'update', 'uses' => 'SubjectTeacherController@update']);
    });

    Route::group([
        'prefix' => 'image',
        'as' => 'image.',
        'namespace' => 'Image',
    ], function () {
        Route::get('{imagableId}',          ['as' => 'index',   'uses' => 'ImageController@index']);
        Route::post('save',                 ['as' => 'save',   'uses' => 'ImageController@save']);
        Route::delete('{imageId}/delete',   ['as' => 'delete',   'uses' => 'ImageController@delete']);
    });

});
