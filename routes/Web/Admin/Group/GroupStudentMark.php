<?php
Route::group([
    'prefix' => 'mark',
    'as' => 'mark.',
], function () {
    Route::post('save',          ['as' => 'save',   'uses' => 'GroupStudentMarkController@save']);
    Route::put('update',        ['as' => 'update', 'uses' => 'GroupStudentMarkController@update']);
});
