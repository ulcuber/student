<?php
Route::group([
    'prefix' => 'hobby',
    'as' => 'hobby.',
], function () {
    Route::post('save',              ['as' => 'save',   'uses' => 'GroupStudentHobbyController@save']);
    Route::put('update',             ['as' => 'update', 'uses' => 'GroupStudentHobbyController@update']);
});
