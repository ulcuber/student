<?php
Route::group([
    'prefix' => '{groupId}/student',
    'as' => 'student.',
], function () {
    Route::get('{studentId?}',              ['as' => 'index',  'uses' => 'GroupStudentController@index'])->where('studentId', '[0-9]+');
    Route::get('create',                    ['as' => 'create', 'uses' => 'GroupStudentController@create']);
    Route::post('save',                     ['as' => 'save',   'uses' => 'GroupStudentController@save']);
    Route::match(['get', 'post'], '{studentId}/edit', ['as' => 'edit',   'uses' => 'GroupStudentController@edit'])->where('studentId', '[0-9]+');
    Route::put('{studentId}/update',        ['as' => 'update', 'uses' => 'GroupStudentController@update'])->where('studentId', '[0-9]+');
    Route::delete('{studentId}/delete',     ['as' => 'delete', 'uses' => 'GroupStudentController@delete'])->where('studentId', '[0-9]+');

    include 'GroupStudentMark.php';
    include 'GroupStudentHobby.php';
});
