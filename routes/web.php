<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

include 'Web/Admin/AdminRoutes.php';

Route::get('/', function () {
    return view('home');
})->name('home');

Route::auth();

Route::get('/get', function () {
    return App\Model\Group\GroupStudent::findOrFail(30904);
});
