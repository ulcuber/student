#!/usr/bin/env bash
# vendor/bin/doctrine orm:convert-mapping --force --from-database yml ./src/doctrine/yaml
vendor/bin/doctrine orm:generate-entities ./src/doctrine/entity --regenerate-entities=REGENERATE_ENTITIES -vvv
# vendor/bin/doctrine orm:schema-tool:update --dump-sql --force
# vendor/bin/doctrine orm:validate-schema
# vendor/bin/doctrine orm:clear-cache:result
# vendor/bin/doctrine orm:clear-cache:query
# vendor/bin/doctrine orm:clear-cache:metadata
