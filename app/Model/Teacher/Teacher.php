<?php
namespace App\Model\Teacher;

use App\Model\Base\Teacher\Teacher as BaseTeacher;

class Teacher extends BaseTeacher
{
    static $listFields = [];

    static $rules = [];

    public function scopeSearch($query, $filterParams)
    {
        if ( isset($filterParams['columns']) && !empty($filterParams['columns']) )
        {
            foreach ($filterParams['columns'] as $key => $name) {
                if ( !empty($name) ) {
                    $query->where($key, 'like', '%'.$name.'%');
                }
            }
        }

        return $query;
    }

    public function scopeSortBy($query, $sortParams)
    {
        $query->orderBy($sortParams['by'], $sortParams['order']);
    }

    public function scopeHaveGroup($query, $groupId)
    {
        if ($groupId) {
            // $query->where('group_id', '=', $groupId);
        }

        return $query;
    }
}
