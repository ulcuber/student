<?php
namespace App\Model\Group;

use App\Model\Base\Group\GroupStudentHobby as BaseGroupStudentHobby;

class GroupStudentHobby extends BaseGroupStudentHobby
{
    static $listFields = [];

    static $rules = [];

    public function scopeInGroup($query, $groupId)
    {
        if ( !empty($groupId) ) {
            $query->whereHas('student', function ($query) use ($groupId) {
                $query->where('group_id', '=', $groupId);
            });
        }

        return $query;
    }
}
