<?php
namespace App\Model\Group;

use App\Model\Base\Group\Group as BaseGroup;

class Group extends BaseGroup
{
    static $listFields = [];

    static $rules = [];

    public function scopeSearch($query, $filterParams)
    {
        if ( isset($filterParams['columns']) && !empty($filterParams['columns']) )
        {
            foreach ($filterParams['columns'] as $key => $name) {
                if ( !empty($name) ) {
                    $query->where($key, 'like', '%'.$name.'%');
                }
            }
        }

        return $query;
    }

    public function scopeSortBy($query, $sortParams)
    {
        $query->orderBy($sortParams['by'], $sortParams['order']);
    }
}
