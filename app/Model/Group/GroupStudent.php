<?php
namespace App\Model\Group;

use App\Model\Base\Group\GroupStudent as BaseGroupStudent;

class GroupStudent extends BaseGroupStudent
{
    static $listFields = [];

    static $rules = [];

    public function scopeSearch($query, $filterParams)
    {
        if ( isset($filterParams['columns']) && !empty($filterParams['columns']) )
        {
            foreach ($filterParams['columns'] as $key => $name) {
                if ( !empty($name) ) {
                    $query->where($key, 'like', '%'.$name.'%');
                }
            }
        }

        if (isset($filterParams['hobbies']) && !empty($filterParams['hobbies']))
        {
            foreach ($filterParams['hobbies'] as $hobby)
            {
                if ( !empty($hobby) ) {
                    $query->whereHas('hobbies', function ($query) use ($hobby) {
                        $query->where('name', 'like', '%'.$hobby.'%');
                    });
                }
            }
        }

        return $query;
    }

    public function scopeSortBy($query, $sortParams)
    {
        $query->orderBy($sortParams['by'], $sortParams['order']);
    }

    public function scopeInGroup($query, $groupId)
    {
        if (isset($groupId) && $groupId != 'all') {
            $query->where('group_id', '=', $groupId);
        }

        return $query;
    }

    public function getFullNameAttribute()
    {
        return $this->surname.' '.$this->name.' '.$this->patronymic;
    }
}
