<?php
namespace App\Model\Base\Subject;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public $timestamps = false;

    protected $table = 'subject';

    protected $fillable = [
        'name',
    ];

    public function marks()
    {
        return $this->hasMany('App\Model\Group\GroupStudentMark', 'subject_id', 'id');
    }

    public function teachers()
    {
        return $this->belongsToMany('App\Model\Teacher\Teacher', 'subject_teacher', 'subject_id', 'id');
    }
}
