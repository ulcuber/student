<?php
namespace App\Model\Base\Group;

use Illuminate\Database\Eloquent\Model;

class GroupStudentHobby extends Model
{
    public $timestamps = false;

    protected $table = 'group__student__hobby';

    protected $fillable = [
        'student_id',
        'name',
    ];

    public function student()
    {
        return $this->belongsTo('App\Model\Group\GroupStudent', 'student_id', 'id');
    }
}
