<?php
namespace App\Model\Base\Group;

use Illuminate\Database\Eloquent\Model;

class GroupStudentMark extends Model
{
    public $timestamps = false;

    protected $table = 'group__student__mark';

    protected $fillable = [
        'value',
        'subject_id',
        'student_id',
    ];

    public function student()
    {
        return $this->belongsTo('App\Model\Group\GroupStudent', 'student_id', 'id');
    }

    public function subject()
    {
        return $this->belongsTo('App\Model\Subject\Subject', 'subject_id', 'id');
    }
}
