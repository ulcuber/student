<?php
namespace App\Model\Base\Group;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public $timestamps = false;

    protected $table = 'group';

    protected $fillable = [
        'name',
        'description',
    ];

    public $avg = 0;
    public $avgs = [];

    public function students()
    {
        return $this->hasMany('App\Model\Group\GroupStudent', 'group_id', 'id');
    }
}
