<?php
namespace App\Model\Base\Group;

use Illuminate\Database\Eloquent\Model;

class GroupStudent extends Model
{
    public $timestamps = false;

    protected $table = 'group__student';

    protected $fillable = [
        'name',
        'surname',
        'patronymic',
        'group_id',
    ];

    protected $appends = ['full_name'];

    public $avg = 0;
    public $avgs = [];

    public function marks()
    {
        return $this->hasMany('App\Model\Group\GroupStudentMark', 'student_id', 'id');
    }

    public function hobbies()
    {
        return $this->hasMany('App\Model\Group\GroupStudentHobby', 'student_id', 'id');
    }

    public function images()
    {
        return $this->morphMany('App\Model\Image\Image', 'imagable', 'imagable_type', 'imagable_id');
    }

    public function group()
    {
        return $this->belongsTo('App\Model\Group\Group', 'group_id', 'id');
    }

    public function user()
    {
        return $this->morphOne('App\User', 'userable', 'userable_type', 'userable_id');
    }

}
