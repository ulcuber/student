<?php

namespace App\Model\Base\Image;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public $timestamps = true;

    protected $table = 'image';

    protected $fillable = [
        'name',
    ];

    public function imagable()
    {
        return $this->morphTo();
    }
}
