<?php

namespace App\Model\Base\Teacher;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public $timestamps = true;

    protected $table = 'teacher';

    protected $fillable = [
        'name',
        'surname',
        'patronymic',
    ];

    public function image()
    {
        return $this->morphOne('App\Model\Image\Image', 'imagable', 'imagable_type', 'imagable_id');
    }

    public function user()
    {
        return $this->morphOne('App\User', 'userable', 'userable_type', 'userable_id');
    }

    public function subjects()
    {
        return $this->belongsToMany('App\Model\Subject\Subject', 'subject_teacher', 'teacher_id', 'id');
    }

}
