<?php

namespace App\Model\Image;

use App\Model\Base\Image\Image as BaseImage;

class Image extends BaseImage
{
    public function src($dir, $absolute = false)
    {
        $src = [];

        preg_match('/(?<=\\\)[^\\\]*\z/', $this->imagable_type, $matches);

        $src[] = $matches[0];

        $src[] = $this->imagable_id;

        $src[] = $dir;

        $src[] = $this->name;

        return (($absolute) ? public_path() : '') . '/image/' . implode('/', $src);
    }

}
