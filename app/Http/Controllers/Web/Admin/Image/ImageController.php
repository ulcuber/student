<?php

namespace App\Http\Controllers\Web\Admin\Image;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Group\GroupStudent;
use App\Model\Image\Image;

class ImageController extends Controller
{
    public function index(Request $request, $imagableId)
    {
        $images = Image::where('imagable_id', $imagableId)->get();

        $files = [];

        foreach($images as $image)
        {
            $path = $image->src('original', true);
            $url = url( $image->src('original') );

            $fileInfo['name'] = $image->name;
            $fileInfo['size'] = file_exists($path) ? filesize($path) : '0';
            $fileInfo['type'] = 'image/*';
            $fileInfo['url'] = $url;
            $fileInfo['thumbnailUrl'] = url( $image->src('100x100') );
            $fileInfo['deleteUrl'] = route('admin.image.delete', ['imageId' => $image->id]);
            $fileInfo['deleteType'] = 'DELETE';
            $files[] = $fileInfo;
        }

        $data['files'] = $files;

        if ( $request->ajax() ) {
            return $data;
//                response()->json($data);
        } else {
            return redirect()->back();
        }
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'imagable_type' => 'required',
            'imagable_id' => 'required|numeric',
            'images' => 'required',
        ]);
        $errors = $validator->errors();

        if ( !$validator->fails() ) {

        $manager = new \Intervention\Image\ImageManager(array('driver' => 'gd'));

        $files = [];

        foreach ($request->file('images') as $file)
        {
            if ( $file->isValid() ) {
            $ext = $file->extension();
            if ($ext == 'jpeg' || $ext == 'png') {

                    $manager = new \Intervention\Image\ImageManager(array('driver' => 'gd'));

                    $image = new Image();

                    $image->imagable_id = $request->input('imagable_id');
                    $image->imagable_type = $request->input('imagable_type');
                    $name = $file->hashName();
                    $image->name = $name;

                    $originalSrc = $image->src('original', true);

                    $file->move(dirname($originalSrc), basename($originalSrc));

                    $thumbnailUrl = [];
                    foreach (config('thumbnail') as $dir => $size)
                    {
                        $tumbnailSrc = $image->src($dir, true);
                        $thumbnailUrl[] = url( $image->src($dir) );

                        if (!is_dir(dirname($tumbnailSrc))) {
                            mkdir(dirname($tumbnailSrc));
                        }

                        $imgSrc = $manager->make($originalSrc)
                            ->resize($size['width'], null, function ($constraint) {
                                $constraint->aspectRatio();
                            })
                            ->save($tumbnailSrc);
                    }

                    $image->save();

                    $url = url( $image->src('original') );
                    $fileInfo['name'] = $name;
                    $fileInfo['size'] = $file->getClientSize();
                    $fileInfo['type'] = 'image/'.$ext;
                    $fileInfo['url'] = $url;
                    $fileInfo['thumbnailUrl'] = ($thumbnailUrl[0]) ? $thumbnailUrl[0] : $url;
                    $fileInfo['deleteUrl'] = route('admin.image.delete', ['imageId' => $image->id]);
                    $fileInfo['deleteType'] = 'DELETE';
            } else {
                $fileInfo['name'] = $file->getClientOriginalName();
                $fileInfo['size'] = $file->getClientSize();
                $fileInfo['error'] = 'Filetype not allowed';
            }

            $files[] = $fileInfo;
            } else {
                $fileInfo['name'] = $file->name();
                $fileInfo['size'] = $file->getClientSize();
                $fileInfo['error'] = 'Filetype not valid';

                $files[] = $fileInfo;
            }
        }
        } else {
            $fileInfo['error'] = $errors->all()[0];

            $files[] = $fileInfo;
        }

        $data['files'] = $files;

        if ( $request->ajax() ) {
            return response()->json($data);
        } else {
            return redirect()->back()->with($errors);
        }
    }

    public function delete(Request $request, $imageId)
    {
        $image = Image::findOrFail($imageId);

        $path = $image->src('original', true);
        if (file_exists($path))
        {
            unlink($path);
        }

        foreach (config('thumbnail') as $dir => $size)
        {
            $path = $image->src($dir, true);
            if (file_exists($path))
            {
                unlink($path);
            }
        }

        $data['name'] = $image->name;

        $image->delete();

        if ( $request->ajax() ) {
            return response()->json($data);
        } else {
            return redirect()->back();
        }
    }

}
