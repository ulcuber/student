<?php

namespace App\Http\Controllers\Web\Admin\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Teacher\Teacher;
use App\Model\Subject\Subject;

class SubjectTeacherController extends Controller
{
    public function update(Request $request)
    {
        $subjects = $request->input('subjects');
        $teacher_id = $request->input('teacher_id');

        $teacher = Teacher::find($teacher_id);

        $teacher->subjects()->detach();

        $subjects = Subject::whereIn('id', $subjects)->get();

        if ( !empty($subjects) ) {
            $teacher->subjects()->save($subjects);
        }

        return redirect()->back();
    }
}
