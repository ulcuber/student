<?php

namespace App\Http\Controllers\Web\Admin\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Subject\Subject;
use App\Model\Teacher\Teacher;
use App\Model\Image\Image;

class TeacherController extends Controller
{
    public function index(Request $request, $teacherId = null)
    {
        if ($teacherId === null)
        {
            $sortParams = [];
            $sortParams['by'] = $request->input('by', 'id');
            $sortParams['order'] = $request->input('order', 'desc');

            $searchParams = $request->only('columns');

            $routeParams['teacherId'] = $teacherId;

            $teachers = Teacher::search($searchParams)->sortBy($sortParams)->with('subjects')->paginate(10);
            $teachers->appends($request->all())->links();

            return view('web.admin.teacher.index', [
                'teachers' => $teachers,
                'sortParams' => $sortParams,
                'searchParams' => $searchParams,
                'routeParams' => $routeParams,
            ]);
        } else {
            return view('web.admin.teacher.show', [
                'teacher' => Teacher::where('id', $id)->with('image')->get()[0],
                'subjects' => Subject::all(),
            ]);
        }
    }

    public function create()
    {
        return view('web.admin.teacher.create', [
        ]);
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'surname' => 'required|max:63',
            'name' => 'required|max:63',
            'patronymic' => 'required|max:63',
        ]);

        $teacher = new Teacher([
            'surname' => $request->input('surname'),
            'name' => $request->input('name'),
            'patronymic' => $request->input('patronymic'),
        ]);

        $teacher->save();

        return redirect()->route('admin.teacher.index');
    }

    public function edit($teacherId)
    {
        return view('web.admin.teacher.edit', [
            'teacher' => Teacher::where('id', $teacherId)->with('image')->get()[0],
        ]);
    }

    public function update(Request $request, $teacherId)
    {
        $this->validate($request, [
            'surname' => 'required|max:63',
            'name' => 'required|max:63',
            'patronymic' => 'required|max:63',
            'image' => 'mimes:jpeg,png',
        ]);

        $teacher = Teacher::find($teacherId);

        $teacher->update([
            'surname' => $request->input('surname'),
            'name' => $request->input('name'),
            'patronymic' => $request->input('patronymic'),
        ]);

        return redirect()->route('admin.teacher.index');
    }

    public function delete($teacherId)
    {
        $student = Teacher::where('id', '=', $teacherId);
        $student->delete();

        return redirect()->back();
    }
}
