<?php
namespace App\Http\Controllers\Web\Admin\Group;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Group\Group;
use App\Model\Group\GroupStudent;
use App\Model\Group\GroupStudentMark;
use App\Model\Group\GroupStudentHobby;
use App\Model\Subject\Subject;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function index(Request $request, $groupId = null)
    {
        $sortParams = [];
        $sortParams['by'] = $request->input('by', 'id');
        $sortParams['order'] = $request->input('order', 'desc');

        if ($groupId === null)
        {
            $searchParams = $request->only('columns');

            $groups = Group::search($searchParams)->sortBy($sortParams)->paginate(10);
            $groups->appends($request->all())->links();

            return view('web.admin.group.group.index', [
                'groups' => $groups,
                'sortParams' => $sortParams,
                'searchParams' => $searchParams,
            ]);
        } else {
            return redirect()->back()->withInput()->with(['error' => true, 'message' => 'there one group will be shown']);
        }
    }

    public function create()
    {
//        $this->authorize('teacher');

        return view('web.admin.group.group.create');
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:group|max:63',
            'description' => 'required|max:511',
        ]);

        $group = new Group($request->all());

        $group->save();

        return redirect()->route('admin.group.index');
    }

    public function edit(Request $request, $groupId = null)
    {
        if ( !empty($groupId) ) {
            $sortParams = [];
            $sortParams['by'] = $request->input('by', 'id');
            $sortParams['order'] = $request->input('order', 'desc');

            $routeParams['groupId'] = $groupId;
            $route = 'admin.group.edit';

            $searchParams = $request->only('columns', 'hobbies');

            $group = Group::find($groupId);

            $students = GroupStudent::inGroup($groupId)
                ->search($searchParams)
                ->sortBy($sortParams)
                ->with('marks')
                ->paginate(10);

            $students->appends($request->all())->links();

            $hobbies = GroupStudentHobby::select('name')->distinct()->inGroup($groupId)->get();

            return view('web.admin.group.group.edit', [
                'group' => $group,
                'groupId' => $groupId,
                'students' => $students,
                'subjects' => Subject::all(),
                'hobbies' => $hobbies,
                'sortParams' => $sortParams,
                'searchParams' => $searchParams,
                'routeParams' => $routeParams,
                'route' => $route,
            ]);
        } else {
            return redirect()->back()->withInput()->with(['error' => true, 'message' => 'groupId is empty']);
        }
    }

    public function update(Request $request, $groupId = null)
    {
        if ( !empty($groupId) ) {
            $this->validate($request, [
                'name' => 'required|max:63',
                'description' => 'required|max:511',
            ]);

            $group = Group::find($groupId);

            $group->update($request->all());

            if ($request->ajax()) {

            $res = view('response', [
                'alert' => 'success',
                'text' => 'Saved',
                'clearId' => 'group-main-content-response',
            ])->render();

            $data['inner'] = [
                'group-main-content-response' => $res,
            ];

            return response()->json($data);
            }

            return redirect()->route('admin.group.index');
        } else {
            return redirect()->back()->withInput()->with(['error' => true, 'message' => 'groupId is empty']);
        }
    }

    public function delete($groupId = null)
    {
        if ( !empty($groupId) ) {
            $group = Group::where('id', '=', $groupId);
            $group->delete();

            return redirect()->back();
        } else {
            return redirect()->back()->withInput()->with(['error' => true, 'message' => 'groupId is empty']);
        }
    }
}
