<?php
namespace App\Http\Controllers\Web\Admin\Group;

use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Filesystem\Filesystem as File;

use App\Model\Group\GroupStudent;
use App\Model\Group\GroupStudentMark;
use App\Model\Group\GroupStudentHobby;
use App\Model\Subject\Subject;
use App\Model\Image\Image;


class GroupStudentController extends Controller
{
    public function index(Request $request, $groupId, $studentId = null)
    {
        if ($studentId === null)
        {
            $sortParams = [];
            $sortParams['by'] = $request->input('by', 'id');
            $sortParams['order'] = $request->input('order', 'desc');

            $routeParams['groupId'] = $groupId;
            $routeParams['studentId'] = $studentId;
            $route = 'admin.group.student.index';

            $searchParams = $request->only('columns', 'hobbies');

            $students = GroupStudent::inGroup($groupId)
                ->search($searchParams)
                ->sortBy($sortParams)
                ->with('marks')
                ->paginate(10);

            $students->appends($request->all());

            $hobbies = GroupStudentHobby::select('name')->distinct()->inGroup($groupId)->get();

            return view('web.admin.group.student.index', [
                'students' => $students,
                'groupId' => $groupId,
                'subjects' => Subject::all(),
                'hobbies' => $hobbies,
                'sortParams' => $sortParams,
                'searchParams' => $searchParams,
                'routeParams' => $routeParams,
                'route' => $route,
            ]);
        } else {
            return view('web.admin.group.student.show', [
                'student' => GroupStudent::where('id', $studentId)->with('marks', 'hobbies', 'image')->get()[0],
                'subjects' => Subject::all(),
            ]);
        }
    }

    public function create($groupId)
    {
        return view('web.admin.group.student.create', [
            'groupId' => $groupId,
        ]);
    }

    public function save(Request $request, $groupId)
    {
        $validator = Validator::make($request->all(), [
            'surname' => 'required|max:63',
            'name' => 'required|max:63',
            'patronymic' => 'required|max:63',
        ]);
        if ( $validator->fails() ) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $student = new GroupStudent([
            'surname' => $request->input('surname'),
            'name' => $request->input('name'),
            'patronymic' => $request->input('patronymic'),
            'group_id' => $request->input('group_id'),
        ]);

        $student->save();

        return redirect()
            ->route('admin.group.student.edit', ['groupId' => $groupId, 'studentId' => $student->id]);
    }

    public function edit(Request $request, $groupId, $studentId)
    {
        $routeParams['groupId'] = $groupId;
        $routeParams['studentId'] = $studentId;

        $student = GroupStudent::where('id', $studentId)->with('marks', 'hobbies', 'images')->get()[0];

        if ( $request->ajax() ) {
            $layout='ajax';
        } else {
            $layout='app';
        }

        return view('web.admin.group.student.edit', [
            'layout' => $layout,
            'student' => $student,
            'routeParams' => $routeParams,
            'subjects' => Subject::all(),
            'hobbies' => GroupStudentHobby::select('name')->distinct()->get(),
        ]);
    }

    public function update(Request $request, $groupId, $studentId)
    {
        $this->validate($request, [
            'surname' => 'required|max:63',
            'name' => 'required|max:63',
            'patronymic' => 'required|max:63',
        ]);

        $student = GroupStudent::find($studentId);

        $student->update([
            'surname' => $request->input('surname'),
            'name' => $request->input('name'),
            'patronymic' => $request->input('patronymic'),
        ]);

        if ($request->ajax()) {

            $res = view('response', [
                'alert' => 'success',
                'text' => 'Saved',
                'clearId' => 'student-main-content-response',
            ])->render();

            $data['inner'] = [
                'student-main-content-response' => $res,
            ];

            return response()->json($data);
        } else {
            return redirect()->back();
        }
    }

    public function delete($groupId, $studentId)
    {
        $student = GroupStudent::where('id', '=', $studentId);
        $student->delete();

        return redirect()->back();
    }
}
