<?php
namespace App\Http\Controllers\Web\Admin\Group;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Group\GroupStudent;
use App\Model\Group\GroupStudentMark;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class GroupStudentMarkController extends Controller {

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'value' => 'required|numeric|min:1|max:5',
            'student_id' => 'required|numeric',
            'subject_id' => 'required|numeric',
        ]);
        $errors = $validator->errors();

        if ( !$validator->fails() ) {
            $mark = new GroupStudentMark($request->all());
            $mark->save();
        }

        if ($request->input('ajax') == true) {
            $student = GroupStudent::where('id', $request->studentId)->with('marks')->get()[0];

            $data = self::makeJSON($student, $request->input('subject_id'), $errors);

            return response()->json($data);
        } else {
            return redirect()->back();
        }
    }

    public function update(Request $request)
    {
        $marks = $request->input('marks');

        if ( isset($marks) ) {
            foreach ($marks as $id => $value)
            {
                if ( !empty($value) ) {
                    if ($value > 0 && $value <= 5)
                    {
                        GroupStudentMark::findOrFail($id)->update(['value' => $value]);
                    }
                } else {
                    GroupStudentMark::destroy($id);
                }
            }
        }

        if ($request->input('ajax') == true) {
            $student = GroupStudent::where('id', $request->studentId)->with('marks')->get()[0];

            $data = self::makeJSON($student, $request->input('subject_id') );

            return response()->json($data);
        } else {
            return redirect()->back();
        }
    }

    private function makeJSON($student, $id, $errors = null)
    {
        $data['input'] = [
            'subject-'.$id.'-mark-add' => '',
        ];

        $subjectMarksList = view('web.admin.group.student.subjectMarks', [
            'marks' => $student->marks->where('subject_id', $id),
        ])->render();
        if (isset($errors) && count($errors) > 0) {
            $subjectMarksAlert = view('error', [
                'errors' => $errors,
            ])->render();
        } else {
            $subjectMarksAlert = view('response', [
                'alert' => 'success',
                'text' => 'Saved',
                'clearId' => 'subject-'.$id.'-alert',
            ])->render();
        }

        $data['inner'] = [
            'subject-'.$id.'-marks' => $subjectMarksList,
            'subject-'.$id.'-alert' => $subjectMarksAlert,
        ];
        return $data;
    }

}
