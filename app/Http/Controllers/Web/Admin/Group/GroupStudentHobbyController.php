<?php
namespace App\Http\Controllers\Web\Admin\Group;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Group\GroupStudent;
use App\Model\Group\GroupStudentHobby;
use Illuminate\Http\Request;

class GroupStudentHobbyController extends Controller
{
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:63',
            'student_id' => 'required|numeric',
        ]);
        $errors = $validator->errors();

        if ( !$validator->fails() ) {
            $hobby = new GroupStudentHobby($request->all());
            $hobby->save();
        }

        if ($request->input('ajax') == true) {
            $student = GroupStudent::where('id', $request->student_id)->with('hobbies')->get()[0];

            $data = self::makeJSON($student, $errors);

            return response()->json($data);
        } else {
            return redirect()->back();
        }
    }

    public function update(Request $request)
    {
        $hobbies = $request->input('hobbies');

        foreach ($hobbies as $id => $name)
        {
            $hobby = GroupStudentHobby::find($id);
            if ( isset($hobby) ) {
                if ( !empty($name) )
            {
                $hobby->name = $name;
                $hobby->save();
            } else {
                $hobby->delete();
            }
            }
        }

        if ($request->input('ajax') == true) {
            $student = GroupStudent::where('id', $request->studentId)->with('hobbies')->get()[0];

            $data = self::makeJSON($student);

            return response()->json($data);
        } else {
            return redirect()->back();
        }
    }

    private function makeJSON($student, $errors = null)
    {
        $data['input'] = [
            'student-hobby-add' => '',
        ];

        $studentHobbiesList = view('web.admin.group.student.studentHobbiesList', [
            'hobbies' => $student->hobbies,
        ])->render();
        if (isset($errors) && count($errors) > 0) {
            $studentHobbiesAlert = view('error', [
                'errors' => $errors,
            ])->render();
        } else {
            $studentHobbiesAlert = view('response', [
                'alert' => 'success',
                'text' => 'Saved',
                'clearId' => 'student-hobbies-alert',
            ])->render();
        }

        $data['inner'] = [
            'student-hobbies-list' => $studentHobbiesList,
            'student-hobbies-alert' => $studentHobbiesAlert,
        ];
        return $data;
    }
}
