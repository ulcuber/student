<?php
namespace App\Http\Controllers\Web\Face\Subject;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Subject\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller {

    public function index()
    {
        $subjects = Subject::orderBy('id', 'desc')->paginate(10);

        return view('web.face.subject.subject.index', [
            'subjects' => $subjects
        ]);
    }

    public function create()
    {
        return view('web.face.subject.subject.create');
    }

    public function save(Request $request)
    {
        $subject = new Subject($request->all());

        $subject->save();

        return redirect()->route('face.subject.index');
    }

    public function edit($id)
    {
        $subject = Subject::find($id);

        return view('web.face.subject.subject.edit', [
            'subject' => $subject
        ]);
    }

    public function update(Request $request, $id)
    {
        $subject = Subject::find($id);

        $subject->update($request->all());

        return redirect()->route('face.subject.index');
    }

    public function delete($id)
    {
        $subject = Subject::where('id', '=', $id);
        $subject->delete();

        return redirect()->back();
    }
}
