<?php
namespace App\Http\Controllers\Web\Face\Group;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Group\GroupStudentMark;
use Illuminate\Http\Request;

class GroupStudentMarkController extends Controller {

    public function index()
    {
        $marks = GroupStudentMark::orderBy('id', 'desc')->paginate(10);

        return view('web.face.group.mark.index', [
            'marks' => $marks
        ]);
    }

    public function create()
    {
        return view('web.face.group.mark.create');
    }

    public function save(Request $request)
    {
        $mark = new GroupStudentMark($request->all());

        $mark->save();

        return redirect()->route('face.mark.index');
    }

    public function edit($id)
    {
        $mark = GroupStudentMark::find($id);

        return view('web.face.group.mark.edit', [
            'mark' => $mark
        ]);
    }

    public function update(Request $request, $id)
    {
        $mark = GroupStudentMark::find($id);

        $mark->update($request->all());

        return redirect()->route('face.mark.index');
    }

    public function delete($id)
    {
        $mark = GroupStudentMark::where('id', '=', $id);
        $mark->delete();

        return redirect()->back();
    }
}
