<?php
namespace App\Http\Controllers\Web\Face\Group;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Group\Group;
use Illuminate\Http\Request;

class GroupController extends Controller {

    public function index()
    {
        $groups = Group::orderBy('id', 'desc')->paginate(10);

        return view('web.face.group.group.index', [
            'groups' => $groups
        ]);
    }

    public function create()
    {
        return view('web.face.group.group.create');
    }

    public function save(Request $request)
    {
        $group = new Group($request->all());

        $group->save();

        return redirect()->route('face.group.index');
    }

    public function edit($id)
    {
        $group = Group::find($id);

        return view('web.face.group.group.edit', [
            'group' => $group
        ]);
    }

    public function update(Request $request, $id)
    {
        $group = Group::find($id);

        $group->update($request->all());

        return redirect()->route('face.group.index');
    }

    public function delete($id)
    {
        $group = Group::where('id', '=', $id);
        $group->delete();

        return redirect()->back();
    }
}
