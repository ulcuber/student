<?php
namespace App\Http\Controllers\Web\Face\Group;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Group\GroupStudent;
use Illuminate\Http\Request;

class GroupStudentController extends Controller {

    public function index()
    {
        $students = GroupStudent::orderBy('id', 'desc')->paginate(10);

        return view('web.face.group.student.index', [
            'students' => $students
        ]);
    }

    public function create()
    {
        return view('web.face.group.student.create');
    }

    public function save(Request $request)
    {
        $student = new GroupStudent($request->all());

        $student->save();

        return redirect()->route('face.student.index');
    }

    public function edit($id)
    {
        $student = GroupStudent::find($id);

        return view('web.face.group.student.edit', [
            'student' => $student
        ]);
    }

    public function update(Request $request, $id)
    {
        $student = GroupStudent::find($id);

        $student->update($request->all());

        return redirect()->route('face.student.index');
    }

    public function delete($id)
    {
        $student = GroupStudent::where('id', '=', $id);
        $student->delete();

        return redirect()->back();
    }
}
