<?php
namespace App\Http\Controllers\Web\Face\Group;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Group\GroupStudentHobby;
use Illuminate\Http\Request;

class GroupStudentHobbyController extends Controller {

    public function index()
    {
        $hobbys = GroupStudentHobby::orderBy('id', 'desc')->paginate(10);

        return view('web.face.group.hobby.index', [
            'hobbys' => $hobbys
        ]);
    }

    public function create()
    {
        return view('web.face.group.hobby.create');
    }

    public function save(Request $request)
    {
        $hobby = new GroupStudentHobby($request->all());

        $hobby->save();

        return redirect()->route('face.hobby.index');
    }

    public function edit($id)
    {
        $hobby = GroupStudentHobby::find($id);

        return view('web.face.group.hobby.edit', [
            'hobby' => $hobby
        ]);
    }

    public function update(Request $request, $id)
    {
        $hobby = GroupStudentHobby::find($id);

        $hobby->update($request->all());

        return redirect()->route('face.hobby.index');
    }

    public function delete($id)
    {
        $hobby = GroupStudentHobby::where('id', '=', $id);
        $hobby->delete();

        return redirect()->back();
    }
}
