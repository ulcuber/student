<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class VictorHelloCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'victor:hello';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hello, world!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $answer = $this->anticipate('Choose one', ['first', 'second', 'third']);
//        $answer = $this->choice('Choose one', ['first', 'second', 'third']);

//        $this->info('Info');
//        $this->question('Question');
//        $this->comment('Comment');
//        $this->error('Error');
//        $this->line('Line');

//        $this->table(['key', 'value'], [['0', 'Zero'], ['1', 'One']]);
        $total = 100;
        $this->output->progressStart($total);

        for ($i = 0; $i < $total; ++$i)
        {
            sleep(1);
            $this->output->progressAdvance();
        }

        $this->output->progressFinish();
    }
}
