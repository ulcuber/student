let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js/');
//mix.js('resources/assets/js/fileupload.js', 'public/js/');
/*
mix.scripts([
    'resources/assets/js/blueimp-file-upload/js/vendor/jquery.ui.widget.js',
    'resources/assets/js/blueimp-file-upload/node_modules/blueimp-tmpl/js/tmpl.js',
    'resources/assets/js/blueimp-file-upload/node_modules/blueimp-load-image/js/load-image.all.min.js',
    'resources/assets/js/blueimp-file-upload/node_modules/blueimp-canvas-to-blob/js/canvas-to-blob.js',
    'resources/assets/js/blueimp-file-upload/node_modules/blueimp-gallery/js/jquery.blueimp-gallery.min.js',
    'resources/assets/js/blueimp-file-upload/js/jquery.iframe-transport.js',
    'resources/assets/js/blueimp-file-upload/js/jquery.fileupload.js',
    'resources/assets/js/blueimp-file-upload/js/jquery.fileupload-process.js',
    'resources/assets/js/blueimp-file-upload/js/jquery.fileupload-image.js',
    'resources/assets/js/jquery.fileupload-ui.js',
    'resources/assets/js/jquery.iframe-transport.js',
], 'public/js/fileupload.js');
*/
/*
mix.styles([
    'resources/views/vendor/blade/html_assets/css/bootstrap.min.css',
], 'public/css/app.css');
mix.styles([
    'public/css/vendor/jquery.fileupload.css',
    'public/css/vendor/jquery.fileupload-ui.css',
    'public/css/vendor/blueimp-gallery.min.css',
], 'public/css/fileupload.css');
*/
