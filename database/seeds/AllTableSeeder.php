<?php

use Illuminate\Database\Seeder;
use App\model\Group\Group;
use App\model\Group\GroupStudent;
use App\model\Group\GroupStudentMark;
use App\model\Group\GroupStudentHobby;
use App\model\Subject\Subject;

class AllTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subjects = Subject::all();

        factory(Group::class, 100)->create()
        ->each(function ($group) use ($subjects) {
            factory(GroupStudent::class, 300)->create(['group_id' => $group->id])
            ->each(function ($student) use ($subjects) {
                $student->hobbies()->save(factory(GroupStudentHobby::class)->make());
                foreach ($subjects as $subject) {
                    factory(GroupStudentMark::class, 5)->create(['subject_id' => $subject->id, 'student_id' => $student->id]);
                }
            });
        });
    }

}
