<?php

use Illuminate\Database\Seeder;
use App\model\Teacher\Teacher;

class TeacherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i<=3; ++$i) {
            factory(Teacher::class, 10)->create();
                // ->each(function ($teacher) use ($i) {
                    // $teacher->subjects()->attach($i);
                // });
        }
    }
}
