<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\model\Group\Group::class, function (Faker\Generator $faker) {
    return[
        'name' => $faker->unique()->word,
        'description' => $faker->unique()->sentence,
    ];
});

$factory->define(App\model\Group\GroupStudent::class, function (Faker\Generator $faker) {
    return[
        'name' => $faker->firstName,
        'surname' => $faker->lastName                                  ,
        'patronymic' => $faker->lastName                                  ,
    ];
});

$factory->define(App\model\Group\GroupStudentMark::class, function (Faker\Generator $faker) {
    return[
        'value' => $faker->numberBetween($min = 2, $max = 5),
    ];
});

$factory->define(App\model\Group\GroupStudentHobby::class, function (Faker\Generator $faker) {
    return[
        'name' => $faker->randomElement(['speedcubing' ,'programming', 'skating', 'swimming']),
    ];
});

$factory->define(App\model\Teacher\Teacher::class, function (Faker\Generator $faker) {
    return[
        'name' => $faker->firstName,
        'surname' => $faker->lastName                                  ,
        'patronymic' => $faker->lastName                                  ,
    ];
});
