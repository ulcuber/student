<?php

return [

// Префиксы
    'prefix' => [
        'api' => [
            'Admin',
            'Face'
        ],
        'web' => [
            'Admin',
            'Face'
        ]
    ],

    // Основные сущности
    'masters' => [
        'Group',
        'Subject',
        'GroupStudentHobby',
        'GroupStudentMark',
    ],

    // Подчинённые сущности
    'subordinates' => [

        'Group' => [
            'GroupStudent',
        ],
    ],


    'options' => [
        '--force' => true, //default false even not exist here
    ],

];
