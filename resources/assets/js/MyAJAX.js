(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define([], factory);
    } else if (typeof exports === 'object') {
        factory();
    } else {
        factory(window.MyAJAX);
    }
}(function () {
'use strict';
var main=this;
/**
 * Activate scripts from element
 */
main.mkS=function(l)
{
    var s=l.getElementsByTagName('script');
    for(var i=0,c;c=s[i];++i)
    {
        var src=c.getAttribute('src');
        if(src!==null && src!==undefined)
        {
            var ch=document.createElement("script");
            ch.async=false;
            ch.src=src;
            ch.type="application/javascript";
            document.head.appendChild(ch);
            ch.parentNode.removeChild(ch);
        }
        else
        {
            var tp=c.getAttribute('type');
            if(tp===null || tp===undefined || tp.toLowerCase()=='application/javascript' || tp.toLowerCase()=='text/javascript')
            {
                eval(c.innerHTML);
            }
        }
    }
}
/**
 * Request function
 * @param string/pointer el: url or HTML-object of form/link
 * @param string res: id of element for response or error
 * @param object data: key-value pairs to send on server also with form data
 * @param bool ns: set `true` to use AJAX even if window.history.pushState undefined (IE>9)
 * @return bool: Returns `false` to prevent default behavior of link/form
 */
main.rq=function(el,res,data,ns)
{
    var url='';
    var error_text='';
    var body=[];
    /*Составление тела запроса*/
    var b="---------------------------"+String(Math.random()).slice(2);
    if(el!=null)
    {
        if(typeof el=='string')
        {
            url=el;
        }
        else
        {
            url=el.getAttribute('action');
            if(url!==null && url!=='')
            {
                var els=el.elements;
                for(var i=0;i<els.length;++i)
                {
                    var nm=els[i].getAttribute('name');
                    if (nm===null || nm===''){continue;}
                    var t=els[i].nodeName.toUpperCase()==='INPUT'?els[i].getAttribute('type').toLowerCase():'text';
                    switch(t)
                    {
                        case 'file':
                            console.log('Form have input of type="file"');
                            return true;
                            break;
                        case 'checkbox':case 'radio':
                            if(els[i].checked)
                            {
                                body.push("Content-Disposition: form-data; name=\""+els[i].name+"\"\r\n\r\n"+els[i].value+"\r\n");
                            }
                            break;
                        default:
                            body.push("Content-Disposition: form-data; name=\""+els[i].name+"\"\r\n\r\n"+els[i].value+"\r\n");
                            break;
                    }
                }
            }
            else
            {
                url=el.getAttribute('href');
                error_text="Попробуйте <a href='"+url+"'>Обновить страницу</a><br>";
                if(url!==null && url!=='')
                {
                    if(window.history.pushState)
                    {
                        if(url != window.location.href)
                        {
                            var h=el.getAttribute('data-h');
                            if(h==true)
                            {
                                /*IE>9*/
                                window.history.pushState({"res": res, "ns": ns}, null, url);
                            }
                        }
                    }
                    else
                    {
                        console.log('window.history.pushState undefined');
                        if(!ns) return true;
                    }
                }
            }
        }
    }

    if(data!=null)
    {
        for (var name in data)
        {
            body.push('Content-Disposition: form-data; name="'+name+'"\r\n\r\n'+data[name]+'\r\n');
        }
    }

    if(url!==null && url!=='')
    {
        var xhttp;
        try
        {
            xhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e)
        {
            try
            {
                xhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (E)
            {
                xhttp=false;
            }
        }
        if (!xhttp && typeof XMLHttpRequest!==undefined)
        {
            xhttp=new XMLHttpRequest();
        }

        xhttp.open('POST', url, true);
        xhttp.setRequestHeader('Content-Type','multipart/form-data; boundary='+b);
        xhttp.setRequestHeader('X-Requested-With','XMLHttpRequest');
        if(document.querySelectorAll)
        {
            var a=document.querySelectorAll('meta[name="csrf-token"]');
            if(a.length>0)
            {
                var tk=a[0].getAttribute('content');
                xhttp.setRequestHeader('X-CSRF-TOKEN', tk);
            }
        }
        xhttp.onreadystatechange=function()
        {
            if(xhttp.readyState==4)
            {
                if(xhttp.status==200)
                {
                    try
                    {
                        var r=JSON.parse(xhttp.responseText);
                    }catch(e){}
                    if(r)
                    {
                        var inner=r['inner'];
                        if(inner!==null && inner!==undefined)
                        {
                            for(var id in inner)
                            {
                                var l=document.getElementById(id);
                                l.innerHTML=inner[id];
                                main.mkS(l);
                            }
                        }

                        var input=r['input'];
                        if(input!==null && input!==undefined)
                        {
                            for(var id in input)
                            {
                                var l=document.getElementById(id);
                                l.value=input[id];
                            }
                        }

                        var active=r['active'];
                        if(active!==null && active!==undefined)
                        {
                            if(document.querySelectorAll)
                            {
                                for(var k in active)
                                {
                                    var v=active[k];
                                    var a=document.querySelectorAll('.'+v);
                                    var rgx=new RegExp('[ ]?'+v, 'gim');
                                    for(var i=0;i<a.length;++i)
                                    {
                                        a[i].className=a[i].className.replace(rgx,'');
                                    }
                                    var a=document.querySelectorAll(k);
                                    for(var i=0;i<a.length;++i)
                                    {
                                        a[i].className+=' '+v;
                                    }
                                }
                            }
                        }

                        var title=r['title'];
                        if(title!==null && title!==undefined)
                        {
                            var t=document.getElementsByTagName('title');
                            for(var i=0;i<t.length;++i)
                            {
                                if(t[i].textContent)
                                {
                                    t[i].textContent=title;
                                }
                            }
                        }
                    }
                    else
                    {
                        var r=xhttp.responseText;
                        var l=document.getElementById(res);
                        l.innerHTML=r;
                        main.mkS(l);
                    }
                }
                else
                {
                    error_text+="Ошибка "+xhttp.status+"<br>"+xhttp.statusText;
                    document.getElementById(res).innerHTML=error_text;
                }
            }
        }
        xhttp.send("--"+b+"\r\n"+body.join("--"+b+"\r\n")+"--"+b+"--\r\n");
    }
    else
    {
        console.log('url is null or empty. Check href or action or first param');
    }
    return false;
}

var init=(function()
{
    /*JSON for IE<8*/
    try{
        var ie=JSON.parse('{"ie": "true"}');
    }catch(e){
        var s=document.createElement("script");
        s.src="js/json3.min.js";
        s.type="application/javascript";
        document.head.appendChild(s);
    }
/**
 * AJAX on "Back/Forward" navigation
 * If History API is not support, event onpopstate will not be handled
 */
    if(window.history.pushState)
    {
        window.onpopstate=function(e)
        {
            var res=e.state['res'];
            var ns=e.state['ns'];
            if(res!==null && res!==undefined && e.state!=='')
            {
                var url=window.location.href;
                main.rq(url,res,{ajax: true},ns);
                if(window.history.pushState) return false;
            }
        }
    }
})();

}))
