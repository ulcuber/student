
/**
* First we will load all of this project's JavaScript dependencies which
* includes Vue and other libraries. It is a great starting point when
* building robust, powerful web applications using Vue and Laravel.
*/

require('./bootstrap');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

require('es6-promise/auto');

window.MyAJAX = require('./MyAJAX');

window.Vue = require('vue');

//Vue.component('student-table', require('./components/StudentTable.vue'));
//Vue.component('content', require('./components/Content.vue'));
