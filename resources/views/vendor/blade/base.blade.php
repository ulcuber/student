namespace {{ $namespace }};

@if ($authenticable)
use Illuminate\Foundation\Auth\User as Model;
@else
use Illuminate\Database\Eloquent\Model;
@endif
@if(isset($cascadeSoftDeletes))
use Iatstuti\Database\Support\CascadeSoftDeletes;
@endif
@if(isset($softDeletes))
use Illuminate\Database\Eloquent\SoftDeletes;
@endif

class {{ $class }} extends Model
{
@if(isset($softDeletes) || isset($cascadeSoftDeletes))
    {{ isset($softDeletes) ? "use SoftDeletes" : '' }}{{ isset($cascadeSoftDeletes) ? ", CascadeSoftDeletes" : ''}};

@endif
    public $timestamps = {{ $timestamp ? 'true' : 'false' }};

    protected $table = '{{ $table }}';
@if (isset($cascadeSoftDeletes))

    protected $cascadeDeletes = [
    @foreach($cascadeSoftDeletes as $entry)
    '{{ $entry }}',
    @endforeach
];
@endif

    protected $fillable = [
    @foreach($fillable as $field)
    '{{ $field }}',
    @endforeach
];
<?php $relationships = isset($relationships) ? $relationships : array(); ?>
@forelse($relationships as $name => $rel)

    public function {{ $rel['name'] }}()
    {
@if ($rel['type'] == 'hasMany' || $rel['type'] == 'hasOne')
        return $this->{{ $rel['type'] }}('{{ $rel['path'] }}', '{{ $rel['foreign'] }}', '{{ $rel['local'] }}');
@elseif ($rel['type'] == 'belongsTo')
        return $this->{{ $rel['type'] }}('{{ $rel['path'] }}', '{{ $rel['local'] }}', '{{ $rel['foreign'] }}');
@elseif ($rel['type'] == 'belongsToMany')
        return $this->{{ $rel['type'] }}('{{ $rel['path'] }}', '{{ $rel['relation_table'] }}', '{{ $rel['foreign'] }}', '{{ $rel['local'] }}');
@endif
    }
@empty
@endforelse
}
