{% $doggy %}extends('layouts.app')

{% $doggy %}section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> {% $class %} / Create </h1>
    </div>
{% $doggy %}endsection

{% $doggy %}section('content')
    {% $doggy %}include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('{% $route %}.save') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('{% $route %}.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>

            </form>

        </div>
    </div>
{% $doggy %}endsection
