namespace {{ $namespace }};

use {{ ucfirst($base_model) }} as Base{{ $class }};

class {{ $class }} extends Base{{ $class }}
{
    static $listFields = [];

    static $rules = [];

    public function scopeNoFilter($query)
    {
        return $query;
    }

    public function add{{ $class }}Filter($query, $params)
    {
        /* EXAMPLE
        *    if (isset($params['name']) && !empty($params['name'])) {
        *       $query->where('name', 'LIKE', '%' . $params['name'] . '%');
        *    }
        */

        return $query;
    }

    public function scopeFilter($query, $params)
    {
        $query = {{ $class }}::noFilter($query);
        $query = {{ $class }}::add{{ $class }}Filter($query, $params);

        return $query;
    }
}
