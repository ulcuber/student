@extends('layouts.app')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Subjects
            @if ( Auth::user() )
            <a class="btn btn-success pull-right" href="{{ route('admin.subject.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
            @endif
        </h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($subjects->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            @if ( Auth::user() )
                            <th>ID</th>
                            @endif
                            <th>Name</th>
                            @if ( Auth::user() )
                            <th class="text-right">OPTIONS</th>
                            @endif
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($subjects as $subject)
                            <tr>
                                @if ( Auth::user() )
                                <td>{{ $subject->id }}</td>
                                @endif
                                <td>{{ $subject->name }}</td>
                                @if ( Auth::user() )
                                <td class="text-right">
                                    <a class="btn btn-xs btn-warning" href="{{ route('admin.subject.edit', $subject->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('admin.subject.delete', $subject->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $subjects->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection
