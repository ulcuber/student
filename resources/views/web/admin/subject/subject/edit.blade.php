@extends('layouts.app')

@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Subject / Edit #{{ $subject->id }}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('admin.subject.update', $subject->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('admin.subject.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>

                <div class="form-subject">
                    <label for="subject-name" class="col-sm-3 control-label">Name</label>
                    <div class="col-sm-8">
                        <input type="text" name="name" id="subject-name" class="form-control" value="{{ $subject->name }}">
                    </div>
                </div>

            </form>

        </div>
    </div>
@endsection
