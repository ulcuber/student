<div class="row">
    <div class="col-md-12">
        @if ( !empty($images) )
        @foreach ($images as $image)
        <img src="{{ $image->src('200x200') }}" class="img-responsive img-thumbnail" alt="Responsive image">
        @endforeach
        @endif
    </div>
</div>
