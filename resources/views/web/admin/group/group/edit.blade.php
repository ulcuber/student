@extends('layouts.app')

@section('title')
Group / Edit #{{ $groupId }}
@endsection

@section('css')
<link rel="stylesheet" href="{{ url('css/fileupload.css') }}">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="{{ url('css/jquery.fileupload-noscript.css') }}"></noscript>
<noscript><link rel="stylesheet" href="{{ url('css/jquery.fileupload-ui-noscript.css') }}"></noscript>
@endsection

@section('modal')
<div id="studentEditModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div id="studentEditModalContent" class="modal-content">

    </div>
  </div>
</div>
<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
@endsection

@section('header')
<i class="glyphicon glyphicon-edit"></i> Group / Edit #{{ $groupId }}
<a class="btn btn-link pull-right" href="{{ route('admin.group.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
@endsection

@section('content')
@include('error')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Main content</div>
            <div class="panel-body">
                <div id="group-main-content">
                <form action="{{ route('admin.group.update', ['groupId' => $group->id]) }}" method="POST" class="form-horizontal" v-on:submit.prevent="update">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label for="group-name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-8">
                            <input v-model="name" type="text" name="name" id="group-name" class="form-control" value="{{ $group->name }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="group-description" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-8">
                            <input v-model="description" type="text" name="description" id="group-description" class="form-control" value="{{ $group->description }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>

                    <div id="group-main-content-response"></div>
                </form>
                    </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-xs-10 col-md-offset-0 col-xs-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading">Filters</div>
            <div class="panel-body">
                @include('web.admin.group.student.filter', ['groupId' => $groupId, 'subjects' => $subjects, 'sortParams' => $sortParams, 'searchParams' => $searchParams, 'routeParams' => $routeParams, 'route' => $route])
            </div>
        </div>
    </div>
    <div class="col-md-9 col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Students</div>
            <div class="panel-body">
                @include('web.admin.group.student.main', ['students' => $students, 'subjects' => $subjects, 'groupId' => $groupId,  'sortParams' => $sortParams, 'searchParams' => $searchParams, 'routeParams' => $routeParams, 'route' => $route])
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    const groupMainContent = new Vue({
        el: '#group-main-content',
        data: {!! json_encode($group->toArray()) !!},
        methods: {
            update: function () {
                axios.put('{{ route('admin.group.update', $routeParams) }}', {
                          name: this.name,
                          description: this.description
                          })
                    .then(function (response) {
                    var l=document.getElementById('group-main-content-response');
                    l.innerHTML=response.data.inner['group-main-content-response'];
                    MyAJAX.mkS(l);
                })
                    .catch(function (error) {
                    console.log(error);
                });
            }
        },
    });
</script>
{{--<script src="{{ url('js/fileupload.js') }}"></script>--}}
@include('web.admin.image.createScripts')
@endsection
