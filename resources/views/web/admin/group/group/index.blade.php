@extends('layouts.app')

@section('title')
Groups
@endsection

@section('header')
<i class="glyphicon glyphicon-edit"></i> List of Groups
@endsection

@section('content')
    <div class="row">
        <div class="col-md-2 col-xs-10 col-md-offset-0 col-xs-offset-1">
            <form action="{{ route('admin.group.index') }}" method="GET" class="form-horizontal">
                @foreach ($sortParams as $name => $value)
                    <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                @endforeach

                <div class="form-group">
                    <a class="btn btn-default" href="{{ route('admin.group.index', array_merge(['groupId' => null], $sortParams) ) }}">Show All</a>
                </div>

                <div class="form-group">
                    <label for="student-name">Name</label>
                    <input type="text" name="columns[name]" id="student-name" class="form-control" placeholder="Name" value="<?php if ( !empty($searchParams['columns']['name']) ) {echo $searchParams['columns']['name'];} ?>">
                </div>

                <div class="form-group">
                    <label for="student-description">Description</label>
                    <input type="text" name="columns[description]" id="student-description" class="form-control" placeholder="Description" value="<?php if ( !empty($searchParams['columns']['description']) ) {echo $searchParams['columns']['description'];} ?>">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> Search</button>
                </div>

                <div class="form-group">
                    <a class="btn btn-default" href="{{ route('admin.group.student.index', ['groupId' => 'all']) }}">Show All Students</a>
                </div>

            </form>
        </div>
        <div class="col-md-10 col-sm-12">
            @if($groups->count())
                <div class="table-responsive">
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>
                                @if ($sortParams['by'] == 'id' && $sortParams['order'] == 'asc')
                                <i class="glyphicon glyphicon-sort-by-order"></i>
                                @else
                                <a href="{{ route('admin.group.index', array_merge(['groupId' => null, 'by' => 'id','order' => 'asc'], $searchParams) ) }}"><i class="glyphicon glyphicon-sort-by-order"></i></a>
                                @endif
                                ID
                                @if ($sortParams['by'] == 'id' && $sortParams['order'] == 'desc')
                                <i class="glyphicon glyphicon-sort-by-order-alt"></i>
                                @else
                                <a href="{{ route('admin.group.index', array_merge(['groupId' => null, 'by' => 'id', 'order' => 'desc'], $searchParams) ) }}"><i class="glyphicon glyphicon-sort-by-order-alt"></i></a>
                                @endif
                            </th>
                            <th>
                                @if ($sortParams['by'] == 'name' && $sortParams['order'] == 'asc')
                                <i class="glyphicon glyphicon-sort-by-alphabet"></i>
                                @else
                                <a href="{{ route('admin.group.index', array_merge(['groupId' => null, 'by' => 'name','order' => 'asc'], $searchParams) ) }}"><i class="glyphicon glyphicon-sort-by-alphabet"></i></a>
                                @endif
                                Name
                                @if ($sortParams['by'] == 'name' && $sortParams['order'] == 'desc')
                                <i class="glyphicon glyphicon-sort-by-alphabet-alt"></i>
                                @else
                                <a href="{{ route('admin.group.index', array_merge(['groupId' => null, 'by' => 'name', 'order' => 'desc'], $searchParams) ) }}"><i class="glyphicon glyphicon-sort-by-alphabet-alt"></i></a>
                                @endif
                            </th>
                            <th>
                                @if ($sortParams['by'] == 'description' && $sortParams['order'] == 'asc')
                                <i class="glyphicon glyphicon-sort-by-alphabet"></i>
                                @else
                                <a href="{{ route('admin.group.index', array_merge(['groupId' => null, 'by' => 'description','order' => 'asc'], $searchParams) ) }}"><i class="glyphicon glyphicon-sort-by-alphabet"></i></a>
                                @endif
                                Description
                                @if ($sortParams['by'] == 'description' && $sortParams['order'] == 'desc')
                                <i class="glyphicon glyphicon-sort-by-alphabet-alt"></i>
                                @else
                                <a href="{{ route('admin.group.index', array_merge(['groupId' => null, 'by' => 'description', 'order' => 'desc'], $searchParams) ) }}"><i class="glyphicon glyphicon-sort-by-alphabet-alt"></i></a>
                                @endif
                            </th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($groups as $group)
                            <tr>
                                <td>{{ $group->id }}</td>
                                <td>{{ $group->name }}</td>
                                <td>{{ $group->description }}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-warning" href="{{ route('admin.group.edit', ['groupId' => $group->id]) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('admin.group.delete', ['groupId' => $group->id]) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                {!! $groups->render() !!}
            <a class="btn btn-success pull-right" href="{{ route('admin.group.create') }}"><i class="glyphicon glyphicon-plus"></i> Add new Group</a>
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection

@section('scripts')

@endsection
