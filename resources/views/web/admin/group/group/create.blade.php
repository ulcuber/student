@extends('layouts.app')

@section('title')
Group / Create
@endsection

@section('header')
<i class="glyphicon glyphicon-plus"></i> Group / Create
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('admin.group.save') }}" method="POST" class="form-horizontal">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('admin.group.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>

                <div class="form-group">
                    <label for="group-name" class="col-sm-3 control-label">Name</label>
                    <div class="col-sm-8">
                        <input type="text" name="name" id="group-name" class="form-control" value="{{ old('name') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="group-description" class="col-sm-3 control-label">Description</label>
                    <div class="col-sm-8">
                        <input type="text" name="description" id="group-description" class="form-control" value="{{ old('description') }}">
                    </div>
                </div>

            </form>

        </div>
    </div>
@endsection
