@if($students->count())
<div class="table-responsive">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th>
                    @if ($sortParams['by'] == 'id' && $sortParams['order'] == 'asc')
                    <i class="glyphicon glyphicon-sort-by-order"></i>
                    @else
                    <a href="{{ route($route, array_merge($routeParams, $searchParams, ['by' => 'id','order' => 'asc']) ) }}"><i class="glyphicon glyphicon-sort-by-order"></i></a>
                    @endif
                    ID
                    @if ($sortParams['by'] == 'id' && $sortParams['order'] == 'desc')
                    <i class="glyphicon glyphicon-sort-by-order-alt"></i>
                    @else
                    <a href="{{ route($route, array_merge($routeParams, $searchParams, ['by' => 'id', 'order' => 'desc']) ) }}"><i class="glyphicon glyphicon-sort-by-order-alt"></i></a>
                    @endif
                </th>
                <th>
                    @if ($sortParams['by'] == 'surname' && $sortParams['order'] == 'asc')
                    <i class="glyphicon glyphicon-sort-by-alphabet"></i>
                    @else
                    <a href="{{ route($route, array_merge($routeParams, $searchParams, ['by' => 'surname','order' => 'asc']) ) }}"><i class="glyphicon glyphicon-sort-by-alphabet"></i></a>
                    @endif
                    Surname
                    @if ($sortParams['by'] == 'surname' && $sortParams['order'] == 'desc')
                    <i class="glyphicon glyphicon-sort-by-alphabet-alt"></i>
                    @else
                    <a href="{{ route($route, array_merge($routeParams, $searchParams, ['by' => 'surname', 'order' => 'desc']) ) }}"><i class="glyphicon glyphicon-sort-by-alphabet-alt"></i></a>
                    @endif
                </th>
                <th>
                    @if ($sortParams['by'] == 'name' && $sortParams['order'] == 'asc')
                    <i class="glyphicon glyphicon-sort-by-alphabet"></i>
                    @else
                    <a href="{{ route($route, array_merge($routeParams, $searchParams, ['by' => 'name','order' => 'asc']) ) }}"><i class="glyphicon glyphicon-sort-by-alphabet"></i></a>
                    @endif
                    Name
                    @if ($sortParams['by'] == 'name' && $sortParams['order'] == 'desc')
                    <i class="glyphicon glyphicon-sort-by-alphabet-alt"></i>
                    @else
                    <a href="{{ route($route, array_merge($routeParams, $searchParams, ['by' => 'name', 'order' => 'desc']) ) }}"><i class="glyphicon glyphicon-sort-by-alphabet-alt"></i></a>
                    @endif
                </th>
                <th>
                    @if ($sortParams['by'] == 'patronymic' && $sortParams['order'] == 'asc')
                    <i class="glyphicon glyphicon-sort-by-alphabet"></i>
                    @else
                    <a href="{{ route($route, array_merge($routeParams, $searchParams, ['by' => 'patronymic','order' => 'asc']) ) }}"><i class="glyphicon glyphicon-sort-by-alphabet"></i></a>
                    @endif
                    Patronymic
                    @if ($sortParams['by'] == 'patronymic' && $sortParams['order'] == 'desc')
                    <i class="glyphicon glyphicon-sort-by-alphabet-alt"></i>
                    @else
                    <a href="{{ route($route, array_merge($routeParams, $searchParams, ['by' => 'patronymic', 'order' => 'desc']) ) }}"><i class="glyphicon glyphicon-sort-by-alphabet-alt"></i></a>
                    @endif
                </th>

                <?php $i = 0; ?>
                @foreach ($subjects as $subject)
                <th>{{ $subject->name }}</th>
                <?php
                $name = $subject->name;
                ++$i;
                ?>
                <?php  ?>
                @endforeach

                <th>Avg</th>
                <th class="text-right">OPTIONS</th>
            </tr>
        </thead>

        <tbody>
            <?php $s = 0; ?>
            @foreach($students as $student)
            <?php ++$s; ?>
            <tr id="stid_{{ $student->id }}">
                <td>{{ $student->id }}</td>
                <td>{{ $student->surname }}</td>
                <td>{{ $student->name }}</td>
                <td>{{ $student->patronymic }}</td>

                <?php $zeroAvgs = 0; ?>
                @foreach ($subjects as $subject)
                <td>
                    <?php $name = $subject->name; ?>
                    {{ number_format($student->avgs[$name] = $student->marks->where('subject_id', $subject->id)->where('value', '!=', 0)->avg('value'), 2) }}
                    <?php
                    if($student->avgs[$name]  == 0) { ++$zeroAvgs; }
                    $student->avg += $student->avgs[$name];
                    ?>
                </td>
                @endforeach
                <?php
                $a = $i-$zeroAvgs;
                if($a == 0) $a = 1;
                $student->avg /= $a;

                $color = null;
                if($student->avg == 5)
                {
                    $color = 'success';
                }
                else
                {
                    if($student->avg > 4.5)
                    {
                        $color = 'warning';
                    }
                    else
                    {
                        if($student->avg < 3)
                        {
                            $color = 'danger';
                        }
                    }
                }
                ?>
                @if( !empty($color) )
                <script>
                    document.getElementById("stid_{{ $student->id }}").className+=" {{ $color }}";
                </script>
                @endif

                <td>
                    {{ number_format($student->avg, 2) }}
                </td>

                <td class="text-right">
                    <a data-toggle="modal" data-target="#studentEditModal" data-remote="false" class="btn btn-xs btn-warning" href="{{ route('admin.group.student.edit', ['groupId' => $student->group_id, 'studentId' => $student->id]) }}" v-on:click="edit({{ route('admin.group.student.edit', ['groupId' => $student->group_id, 'studentId' => $student->id]) }})" onclick="MyAJAX.rq(this, 'studentEditModalContent');"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                    <form action="{{ route('admin.group.student.delete', ['groupId' => $student->group_id, 'studentId' => $student->id]) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>
</div>
{{ $students->links() }}
@if ( isset($groupId) && $groupId != 'all')
<a class="btn btn-success pull-right" href="{{ route('admin.group.student.create', ['groupId' => $groupId]) }}"><i class="glyphicon glyphicon-plus"></i> Add Student to Group</a>
@endif
@else
<h3 class="text-center alert alert-info">Empty!</h3>
@endif
