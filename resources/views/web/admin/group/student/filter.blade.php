<form action="{{ route($route, $routeParams) }}" method="GET" class="form-horizontal">
    @foreach ($sortParams as $name => $value)
    <input type="hidden" name="{{ $name }}" value="{{ $value }}">
    @endforeach

    <div class="form-group">
        <div class="col-sm-12">
            <a class="btn btn-default" href="{{ route($route, array_merge($routeParams, $sortParams) ) }}">Remove filters</a>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <label for="student-surname">Surname</label>
            <input type="text" name="columns[surname]" id="student-surname" class="form-control" placeholder="Surname" value="<?php if ( !empty($searchParams['columns']['surname']) ) {echo $searchParams['columns']['surname'];} ?>">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <label for="student-name">Name</label>
            <input type="text" name="columns[name]" id="student-name" class="form-control" placeholder="Name" value="<?php if ( !empty($searchParams['columns']['name']) ) {echo $searchParams['columns']['name'];} ?>">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <label for="student-patronymic">Patronymic</label>
            <input type="text" name="columns[patronymic]" id="student-patronymic" class="form-control" placeholder="Patronymic" value="<?php if ( !empty($searchParams['columns']['patronymic']) ) {echo $searchParams['columns']['patronymic'];} ?>">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <label>Hobby</label>
            @foreach ($hobbies as $hobby)
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="hobbies[]" value="{{ $hobby->name }}"<?php if ( isset($searchParams['hobbies']) && in_array($hobby->name, $searchParams['hobbies']) ) {echo ' checked';} ?>>
                    {{ $hobby->name }}
                </label>
            </div>
            @endforeach
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-8">
            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> Search</button>
        </div>
    </div>

</form>
