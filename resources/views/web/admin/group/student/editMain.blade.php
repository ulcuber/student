<div class="row">
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">Main content</div>
            <div class="panel-body">
                <div id="student-main-content">
                <form action="{{ route('admin.group.student.update', $routeParams) }}" method="POST" class="form-horizontal" v-on:submit.prevent="update">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label for="student-surname" class="col-sm-2 control-label">Surname</label>
                        <div class="col-sm-8">
                            <input v-model="surname" type="text" name="surname" id="student-surname" class="form-control" value="{{ $student->surname }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="student-name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-8">
                            <input v-model="name" type="text" name="name" id="student-name" class="form-control" value="{{ $student->name }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="student-patronymic" class="col-sm-2 control-label">Patronymic</label>
                        <div class="col-sm-8">
                            <input v-model="patronymic" type="text" name="patronymic" id="student-patronymic" class="form-control" value="{{ $student->patronymic }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>

                    <div id="student-main-content-response">

                    </div>

                </form>
            </div>
                </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Hobbies</div>
            <div class="panel-body">
                <form action="{{ route('admin.group.student.hobby.update', $routeParams) }}" method="POST" class="form-horizontal" onsubmit="return MyAJAX.rq(this, 'student-hobbies-alert', {ajax:true});">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div id="student-hobbies-list">
                    @include('web.admin.group.student.studentHobbiesList', ['hobbies' => $student->hobbies])
                    </div>

                    <div class="form-group">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>

                </form>
                <div id="student-hobbies-alert"></div>
                <form action="{{ route('admin.group.student.hobby.save', $routeParams) }}" method="POST" class="form-inline" onsubmit="return MyAJAX.rq(this, 'student-hobbies-alert', {ajax:true});">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="student_id" value="{{ $student->id }}">

                    <div class="form-group">
                        <input id="student-hobby-add" name="name" type="text" class="form-control">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i> Add</button>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Marks</div>
            <div class="panel-body">
                <h4>Avgs</h4>
                <div class="table-responsive">
                    <table class="table table-condensed table-striped">
                        <thead>
                            <tr>
                                <?php $i = 0; ?>
                                @foreach ($subjects as $subject)
                                <th>{{ $subject->name }}</th>
                                <?php
                                $name = $subject->name;
                                ++$i;
                                ?>
                                <?php  ?>
                                @endforeach
                                <th>Avg</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr id="stid_{{ $student->id }}">
                                <?php $zeroAvgs = 0; ?>
                                @foreach ($subjects as $subject)
                                <td>
                                    <?php $name = $subject->name; ?>
                                    {{ number_format($student->avgs[$name] = $student->marks->where('subject_id', $subject->id)->where('value', '!=', 0)->avg('value'), 2) }}
                                    <?php
                                    if($student->avgs[$name]  == 0) { ++$zeroAvgs; }
                                    $student->avg += $student->avgs[$name];
                                    ?>
                                </td>
                                @endforeach
                                <?php
                                $a = $i-$zeroAvgs;
                                if($a == 0) $a = 1;
                                $student->avg /= $a;

                                $color = null;
                                if($student->avg == 5)
                                {
                                    $color = 'lightgreen';
                                }
                                else
                                {
                                    if($student->avg > 4.5)
                                    {
                                        $color = 'lightyellow';
                                    }
                                    else
                                    {
                                        if($student->avg < 3)
                                        {
                                            $color = '#FFCBDB';
                                        }
                                    }
                                }
                                ?>
                                @if( !empty($color) )
                                <script>
                                    document.getElementById("stid_{{ $student->id }}").style.cssText="background-color: {{ $color }}";
                                </script>
                                @endif

                                <td>
                                    {{ number_format($student->avg, 2) }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    @foreach ($subjects as $subject)
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">{{ $subject->name }}</div>
            <div class="panel-body">
                <?php $marks = $student->marks->where('subject_id', $subject->id); ?>
                @if ( $marks->count() )
                <form action="{{ route('admin.group.student.mark.update', $routeParams) }}" method="POST" class="form-inline" onsubmit="return MyAJAX.rq(this, 'subject-{{ $subject->id }}-alert', {ajax:true});">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="subject_id" value="{{ $subject->id }}">

                    <div id="subject-{{ $subject->id }}-marks">
                        @include('web.admin.group.student.subjectMarks')
                    </div>
                    <br>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>

                </form>
                @endif
                <br>
                <div id="subject-{{ $subject->id }}-alert"></div>
                <form action="{{ route('admin.group.student.mark.save', $routeParams) }}" method="POST" class="form-inline" onsubmit="return MyAJAX.rq(this, 'subject-{{ $subject->id }}-alert', {ajax:true});">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="student_id" value="{{ $student->id }}">
                    <input type="hidden" name="subject_id" value="{{ $subject->id }}">

                    <div class="form-group">
                        <input id="subject-{{ $subject->id }}-mark-add" name="value" type="number" min="0" max="5" class="form-control">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i> Add</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    @endforeach
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Add Images</div>
            <div class="panel-body">
                @includeif('web.admin.image.create', ['imagableType' => 'App\Model\Group\GroupStudent', 'imagableId' => $student->id])
            </div>
        </div>
    </div>
</div>

{{--
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Gallery</div>
            <div class="panel-body">
                @includeif('web.admin.image.gallery', ['images' => $student->images])
            </div>
        </div>
    </div>
</div>
--}}
