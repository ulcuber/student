@extends('layouts.'.$layout)

@section('title')
Student / Edit #{{ $student->id }} from Group #{{ $student->group_id }}
@endsection

@section('css')
<link rel="stylesheet" href="{{ url('css/fileupload.css') }}">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="{{ url('css/jquery.fileupload-noscript.css') }}"></noscript>
<noscript><link rel="stylesheet" href="{{ url('css/jquery.fileupload-ui-noscript.css') }}"></noscript>
@endsection

@section('modal')
<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
@endsection

@section('header')
<i class="glyphicon glyphicon-edit"></i> Student / Edit #{{ $student->id }} from Group #{{ $student->group_id }}
@if ($layout!='ajax')
<a class="btn btn-link pull-right" href="{{ route('admin.group.edit', ['groupId' => $student->group_id ]) }}"><i class="glyphicon glyphicon-backward"></i> Back to Group / Edit</a>
@endif
@endsection

@section('content')

@include('error')
@include('web.admin.group.student.editMain')

@endsection

@section('scripts')

@if ($layout!='ajax')
{{--<script src="{{ url('js/fileupload.js') }}"></script>--}}
@include('web.admin.image.createScripts')
@endif

<script>
    $('#studentEditModal').modal('handleUpdate');
    $('#fileupload').fileupload();

    $(function () {
        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: '{{ route('admin.image.index', ['imagableId' => $student->id]) }}',
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
        });

    });

</script>
<script>
    const studentMainContent = new Vue({
        el: '#student-main-content',
        data: {!! json_encode($student->toArray()) !!},
        methods: {
            update: function () {
                axios.put('{{ route('admin.group.student.update', $routeParams) }}', {
                          surname: this.surname,
                          name: this.name,
                          patronymic: this.patronymic
                          })
                    .then(function (response) {
                    var l=document.getElementById('student-main-content-response');
                    l.innerHTML=response.data.inner['student-main-content-response'];
                    MyAJAX.mkS(l);
                })
                    .catch(function (error) {
                    console.log(error);
                });
            }
        },
        template: '#student-main-content',
    });
</script>
@endsection
