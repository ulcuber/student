@foreach ($hobbies as $hobby)
<div class="form-group">
    <div class="col-md-8 col-sm-10 col-xs-12">
        <input name="hobbies[{{ $hobby->id }}]" type="text" value="{{ $hobby->name }}" class="form-control">
    </div>
</div>
@endforeach
