@extends('layouts.app')

@section('title')
Student / Create for Group #{{ $groupId }}
@endsection

@section('header')
<i class="glyphicon glyphicon-plus"></i> Student / Create for Group #{{ $groupId }}
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('admin.group.student.save', ['groupId' => $groupId]) }}" method="POST" class="form-horizontal">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="group_id" id="student-group_id" value="{{ $groupId }}">

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('admin.group.edit', ['groupId' => $groupId]) }}"><i class="glyphicon glyphicon-backward"></i> Back to Group / Edit</a>
                </div>

                <div class="form-group">
                    <label for="student-surname" class="col-sm-3 control-label">Surname</label>
                    <div class="col-sm-8">
                        <input type="text" name="surname" id="student-surname" class="form-control" value="{{ old('surname') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="student-name" class="col-sm-3 control-label">Name</label>
                    <div class="col-sm-8">
                        <input type="text" name="name" id="student-name" class="form-control" value="{{ old('name') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="student-patronymic" class="col-sm-3 control-label">Patronymic</label>
                    <div class="col-sm-8">
                        <input type="text" name="patronymic" id="student-patronymic" class="form-control" value="{{ old('patronymic') }}">
                    </div>
                </div>

            </form>

        </div>
    </div>
@endsection
