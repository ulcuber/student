@extends('layouts.app')

@section('header')

@endsection

@section('content')
@include('error')

<div class="col-lg-12 col-sm-6">
    <div class="card hovercard">
        @if($student->image)
        <div class="card-background">
            <img class="card-bkimg" alt="" src="{{ $student->image->src('100x100') }}">
        </div>
        <div class="useravatar">
            <img alt="" src="{{ $student->image->src('100x100') }}">
        </div>
        @else
        <div class="card-background">
            <img class="card-bkimg" alt="" src="{{ url('image/student/nophoto.png') }}">
        </div>
        <div class="useravatar">
            <img alt="" src="{{ url('image/student/nophoto.png') }}">
        </div>
        @endif
        <div class="card-info">
            <span class="card-title">{{ $student->surname }}</span>
            <span class="card-title">{{ $student->name }}</span>
            <span class="card-title">{{ $student->patronymic }}</span>
        </div>
    </div>
    <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
        <div class="btn-group" role="group">
            <button type="button" id="stars" class="btn btn-primary" href="#tab1" data-toggle="tab"><span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <div class="hidden-xs">Marks</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                <div class="hidden-xs">Hobbies</div>
            </button>
        </div>
    </div>

    <div class="well">
    <a class="btn btn-link pull-right" href="{{ route('admin.group.student.index', $student->group_id) }}"><i class="glyphicon glyphicon-backward"></i> Back to group</a>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="tab1">
                <h3>Marks</h3>
                <h4>Avgs</h4>
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <?php $i = 0; ?>
                            @foreach ($subjects as $subject)
                            <th>{{ $subject->name }}</th>
                            <?php
                            $name = $subject->name;
                            ++$i;
                            ?>
                            <?php  ?>
                            @endforeach
                            <th>Avg</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="stid_{{ $student->id }}">
                            <?php $zeroAvgs = 0; ?>
                            @foreach ($subjects as $subject)
                            <td>
                                <?php $name = $subject->name; ?>
                                {{ number_format($student->avgs[$name] = $student->marks->where('subject_id', $subject->id)->where('value', '!=', 0)->avg('value'), 2) }}
                                <?php
                                if($student->avgs[$name]  == 0) { ++$zeroAvgs; }
                                $student->avg += $student->avgs[$name];
                                ?>
                            </td>
                            @endforeach
                            <?php
                            $a = $i-$zeroAvgs;
                            if($a == 0) $a = 1;
                            $student->avg /= $a;

                            $color = null;
                            if($student->avg == 5)
                            {
                                $color = 'lightgreen';
                            }
                            else
                            {
                                if($student->avg > 4.5)
                                {
                                    $color = 'lightyellow';
                                }
                                else
                                {
                                    if($student->avg < 3)
                                    {
                                        $color = '#FFCBDB';
                                    }
                                }
                            }
                            ?>
                            @if( !empty($color) )
                            <script>
                                document.getElementById("stid_{{ $student->id }}").style.cssText="background-color: {{ $color }}";
                            </script>
                            @endif

                            <td>
                                {{ number_format($student->avg, 2) }}
                            </td>
                        </tr>
                    </tbody>
                </table>

                @foreach ($subjects as $subject)
                <?php $marks = $student->marks->where('subject_id', $subject->id); ?>
                <h4>{{ $subject->name }}</h4>
                @if ( $marks->count() )
                @if ( Auth::user() )
                    <form action="{{ route('admin.mark.update') }}" method="POST" class="form-inline">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        @foreach ($marks as $mark)
                        <div class="form-group">
                            <input name="marks[{{ $mark->id }}]" type="number" min="0" max="5" value="{{ $mark->value }}" class="form-control">
                        </div>
                        @endforeach

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>

                    </form>
                @else
                    @foreach ($marks as $mark)
                    <span>
                        {{ $mark->value }}
                    </span>
                    @endforeach
                @endif
                @endif
                @if ( Auth::user() )
                <form action="{{ route('admin.mark.save') }}" method="POST" class="form-inline">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="student_id" value="{{ $student->id }}">
                    <input type="hidden" name="subject_id" value="{{ $subject->id }}">

                    <div class="form-group">
                        <input name="value" type="number" min="0" max="5" class="form-control">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i> Add</button>
                    </div>

                </form>
                @endif
                @endforeach
            </div>
            <div class="tab-pane fade in" id="tab2">
                <h3>Hobbies</h3>
                @if ( $student->hobbies->count() )
                @if ( Auth::user() )
                    <form action="{{ route('admin.hobby.update') }}" method="POST" class="form-horizontal">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        @foreach ($student->hobbies as $hobby)
                        <div class="form-group">
                            <div class="col-md-6 col-sm-10">
                                <input name="hobbies[{{ $hobby->id }}]" type="text" value="{{ $hobby->name }}" class="form-control">
                            </div>
                        </div>
                        @endforeach

                        <div class="form-group">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>

                    </form>
                @else
                    <ul>
                    @foreach ($student->hobbies as $hobby)
                        <li>{{ $hobby->name }}</li>
                    @endforeach
                    </ul>
                @endif
                @endif
                @if ( Auth::user() )
                <form action="{{ route('admin.hobby.save') }}" method="POST" class="form-inline">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="student_id" value="{{ $student->id }}">

                    <div class="form-group">
                        <input name="name" type="text" class="form-control">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i> Add</button>
                    </div>

                </form>
                @endif
            </div>
        </div>
    </div>

</div>
<style>
    /* USER PROFILE PAGE */
    .card {
        margin-top: 20px;
        padding: 30px;
        background-color: rgba(214, 224, 226, 0.2);
        -webkit-border-top-left-radius:5px;
        -moz-border-top-left-radius:5px;
        border-top-left-radius:5px;
        -webkit-border-top-right-radius:5px;
        -moz-border-top-right-radius:5px;
        border-top-right-radius:5px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    .card.hovercard {
        position: relative;
        padding-top: 0;
        overflow: hidden;
        text-align: center;
        background-color: #fff;
        background-color: rgba(255, 255, 255, 1);
    }
    .card.hovercard .card-background {
        height: 130px;
    }
    .card-background img {
        -webkit-filter: blur(25px);
        -moz-filter: blur(25px);
        -o-filter: blur(25px);
        -ms-filter: blur(25px);
        filter: blur(25px);
        margin-left: -100px;
        margin-top: -200px;
        min-width: 130%;
    }
    .card.hovercard .useravatar {
        position: absolute;
        top: 15px;
        left: 0;
        right: 0;
    }
    .card.hovercard .useravatar img {
        width: 100px;
        height: 100px;
        max-width: 100px;
        max-height: 100px;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        border-radius: 50%;
        border: 5px solid rgba(255, 255, 255, 0.5);
    }
    .card.hovercard .card-info {
        position: absolute;
        bottom: 14px;
        left: 0;
        right: 0;
    }
    .card.hovercard .card-info .card-title {
        padding:0 5px;
        font-size: 20px;
        line-height: 1;
        color: #262626;
        background-color: rgba(255, 255, 255, 0.1);
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    .card.hovercard .card-info {
        overflow: hidden;
        font-size: 12px;
        line-height: 20px;
        color: #737373;
        text-overflow: ellipsis;
    }
    .card.hovercard .bottom {
        padding: 0 20px;
        margin-bottom: 17px;
    }
    .btn-pref .btn {
        -webkit-border-radius:0 !important;
    }

</style>
<script>
    window.onload = function() {
        $(".btn-pref .btn").click(function () {
            $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
            $(this).removeClass("btn-default").addClass("btn-primary");
        });
    };
</script>
@endsection
