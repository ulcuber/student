@foreach ($marks as $mark)
<div class="form-group">
    <input name="marks[{{ $mark->id }}]" type="number" min="0" max="5" value="{{ $mark->value }}" class="form-control">
</div>
@endforeach
