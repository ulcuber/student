@extends('layouts.app')

@section('title')
Students
@endsection

@section('css')
<link rel="stylesheet" href="{{ url('css/fileupload.css') }}">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="{{ url('css/jquery.fileupload-noscript.css') }}"></noscript>
<noscript><link rel="stylesheet" href="{{ url('css/jquery.fileupload-ui-noscript.css') }}"></noscript>
@endsection

@section('modal')
<div id="studentEditModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div id="studentEditModalContent" class="modal-content">

    </div>
  </div>
</div>
<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
@endsection

@section('header')
@if( !empty($groupId) && $groupId != 'all' )
<i class="glyphicon glyphicon-align-justify"></i> Students for Group #{{ $groupId }}
@else
<i class="glyphicon glyphicon-align-justify"></i> All Students
@endif
@endsection

@section('content')
<div class="row">
    <div class="col-md-2 col-xs-10 col-md-offset-0 col-xs-offset-1">
        @include('web.admin.group.student.filter')
    </div>
    <div class="col-md-10 col-sm-12">
        @include('web.admin.group.student.main')
    </div>
</div>
@endsection

@section('scripts')
@include('web.admin.image.createScripts')
@endsection
