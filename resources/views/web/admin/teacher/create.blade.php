@extends('layouts.app')

@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Teacher / Create </h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('admin.teacher.save') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('admin.teacher.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>

                <div class="form-group">
                    <label for="student-surname" class="col-sm-3 control-label">Surname</label>
                    <div class="col-sm-8">
                        <input type="text" name="surname" id="student-surname" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label for="student-name" class="col-sm-3 control-label">Name</label>
                    <div class="col-sm-8">
                        <input type="text" name="name" id="student-name" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label for="student-patronymic" class="col-sm-3 control-label">Patronymic</label>
                    <div class="col-sm-8">
                        <input type="text" name="patronymic" id="student-patronymic" class="form-control">
                    </div>
                </div>

            </form>

        </div>
    </div>
@endsection
