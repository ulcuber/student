@extends('layouts.app')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Teachers
            <a class="btn btn-success pull-right" href="{{ route('admin.teacher.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-2 col-xs-10 col-md-offset-0 col-xs-offset-1">
            <form action="{{ route('admin.teacher.index', $routeParams) }}" method="GET" class="form-horizontal">
                @foreach ($sortParams as $name => $value)
                    <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                @endforeach

                <div class="form-group">
                    <a class="btn btn-default" href="{{ route('admin.teacher.index', array_merge($routeParams, $sortParams) ) }}">Remove filters</a>
                </div>

                <div class="form-group">
                    <label for="teacher-surname">Surname</label>
                    <input type="text" name="columns[surname]" id="teacher-surname" class="form-control" placeholder="Surname" value="<?php if ( !empty($searchParams['columns']['surname']) ) {echo $searchParams['columns']['surname'];} ?>">
                </div>

                <div class="form-group">
                    <label for="teacher-name">Name</label>
                    <input type="text" name="columns[name]" id="teacher-name" class="form-control" placeholder="Name" value="<?php if ( !empty($searchParams['columns']['name']) ) {echo $searchParams['columns']['name'];} ?>">
                </div>

                <div class="form-group">
                    <label for="teacher-patronymic">Patronymic</label>
                    <input type="text" name="columns[patronymic]" id="teacher-patronymic" class="form-control" placeholder="Patronymic" value="<?php if ( !empty($searchParams['columns']['patronymic']) ) {echo $searchParams['columns']['patronymic'];} ?>">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> Search</button>
                </div>

            </form>
        </div>
        <div class="col-md-10 col-sm-12">

            @if($teachers->count())
                <div class="table-responsive">
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>
                                @if ($sortParams['by'] == 'id' && $sortParams['order'] == 'asc')
                                <i class="glyphicon glyphicon-sort-by-order"></i>
                                @else
                                <a href="{{ route('admin.teacher.index', array_merge($routeParams, ['by' => 'id','order' => 'asc'], $searchParams) ) }}"><i class="glyphicon glyphicon-sort-by-order"></i></a>
                                @endif
                                ID
                                @if ($sortParams['by'] == 'id' && $sortParams['order'] == 'desc')
                                <i class="glyphicon glyphicon-sort-by-order-alt"></i>
                                @else
                                <a href="{{ route('admin.teacher.index', array_merge($routeParams, ['by' => 'id', 'order' => 'desc'], $searchParams) ) }}"><i class="glyphicon glyphicon-sort-by-order-alt"></i></a>
                                @endif
                            </th>
                            <th>
                                @if ($sortParams['by'] == 'surname' && $sortParams['order'] == 'asc')
                                <i class="glyphicon glyphicon-sort-by-alphabet"></i>
                                @else
                                <a href="{{ route('admin.teacher.index', array_merge($routeParams, ['by' => 'surname','order' => 'asc'], $searchParams) ) }}"><i class="glyphicon glyphicon-sort-by-alphabet"></i></a>
                                @endif
                                Surname
                                @if ($sortParams['by'] == 'surname' && $sortParams['order'] == 'desc')
                                <i class="glyphicon glyphicon-sort-by-alphabet-alt"></i>
                                @else
                                <a href="{{ route('admin.teacher.index', array_merge($routeParams, ['by' => 'surname', 'order' => 'desc'], $searchParams) ) }}"><i class="glyphicon glyphicon-sort-by-alphabet-alt"></i></a>
                                @endif
                            </th>
                            <th>
                                @if ($sortParams['by'] == 'name' && $sortParams['order'] == 'asc')
                                <i class="glyphicon glyphicon-sort-by-alphabet"></i>
                                @else
                                <a href="{{ route('admin.teacher.index', array_merge($routeParams, ['by' => 'name','order' => 'asc'], $searchParams) ) }}"><i class="glyphicon glyphicon-sort-by-alphabet"></i></a>
                                @endif
                                Name
                                @if ($sortParams['by'] == 'name' && $sortParams['order'] == 'desc')
                                <i class="glyphicon glyphicon-sort-by-alphabet-alt"></i>
                                @else
                                <a href="{{ route('admin.teacher.index', array_merge($routeParams, ['by' => 'name', 'order' => 'desc'], $searchParams) ) }}"><i class="glyphicon glyphicon-sort-by-alphabet-alt"></i></a>
                                @endif
                            </th>
                            <th>
                                @if ($sortParams['by'] == 'patronymic' && $sortParams['order'] == 'asc')
                                <i class="glyphicon glyphicon-sort-by-alphabet"></i>
                                @else
                                <a href="{{ route('admin.teacher.index', array_merge($routeParams, ['by' => 'patronymic','order' => 'asc'], $searchParams) ) }}"><i class="glyphicon glyphicon-sort-by-alphabet"></i></a>
                                @endif
                                Patronymic
                                @if ($sortParams['by'] == 'patronymic' && $sortParams['order'] == 'desc')
                                <i class="glyphicon glyphicon-sort-by-alphabet-alt"></i>
                                @else
                                <a href="{{ route('admin.teacher.index', array_merge($routeParams, ['by' => 'patronymic', 'order' => 'desc'], $searchParams) ) }}"><i class="glyphicon glyphicon-sort-by-alphabet-alt"></i></a>
                                @endif
                            </th>

                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($teachers as $teacher)
                        <tr>
                            <td>{{ $teacher->id }}</td>
                            <td>{{ $teacher->surname }}</td>
                            <td>{{ $teacher->name }}</td>
                            <td>{{ $teacher->patronymic }}</td>

                            <td class="text-right">
                                <a class="btn btn-xs btn-warning" href="{{ route('admin.teacher.edit', ['teacherId' => $teacher->id]) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                <form action="{{ route('admin.teacher.delete', ['teacherId' => $teacher->id]) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                </div>
                {!! $teachers->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection
