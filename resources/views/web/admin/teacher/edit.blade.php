@extends('layouts.app')

@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Teacher / Edit #{{ $teacher->id }}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('admin.teacher.update', $teacher->id) }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('admin.teacher.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>

                <div class="form-group">
                    <label for="student-surname" class="col-sm-3 control-label">Surname</label>
                    <div class="col-sm-8">
                        <input type="text" name="surname" id="student-surname" class="form-control" value="{{ $teacher->surname }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="student-name" class="col-sm-3 control-label">Name</label>
                    <div class="col-sm-8">
                        <input type="text" name="name" id="student-name" class="form-control" value="{{ $teacher->name }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="student-patronymic" class="col-sm-3 control-label">Patronymic</label>
                    <div class="col-sm-8">
                        <input type="text" name="patronymic" id="student-patronymic" class="form-control" value="{{ $teacher->patronymic }}">
                    </div>
                </div>

            </form>

        </div>
    </div>
@endsection
