@extends('layouts.app')

@section('header')

@endsection

@section('content')
@include('error')

<div class="col-lg-12 col-sm-6">
    <div class="card hovercard">
        @if($teacher->image)
        <div class="card-background">
            <img class="card-bkimg" alt="" src="{{ $teacher->image->src('100x100') }}">
        </div>
        <div class="useravatar">
            <img alt="" src="{{ $teacher->image->src('100x100') }}">
        </div>
        @else
        <div class="card-background">
            <img class="card-bkimg" alt="" src="{{ url('image/student/nophoto.png') }}">
        </div>
        <div class="useravatar">
            <img alt="" src="{{ url('image/student/nophoto.png') }}">
        </div>
        @endif
        <div class="card-info">
            <span class="card-title">{{ $teacher->surname }}</span>
            <span class="card-title">{{ $teacher->name }}</span>
            <span class="card-title">{{ $teacher->patronymic }}</span>
        </div>
    </div>
    <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
        <div class="btn-group" role="group">
            <button type="button" id="stars" class="btn btn-primary" href="#tab1" data-toggle="tab"><span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <div class="hidden-xs">Subjects</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                <div class="hidden-xs">Biography</div>
            </button>
        </div>
    </div>

    <div class="well">
    <a class="btn btn-link pull-right" href="{{ route('admin.teacher.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="tab1">
                <h3>Subjects</h3>
                @if ( Auth::user() )
                    <form action="{{ route('admin.subjectTeacher.update') }}" method="POST" class="form-horizontal">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="teacher_id" value="{{ $teacher->id }}">

                        <div class="form-group">
                            <div class="col-md-2 col-xs-10 col-md-offset-0 col-xs-offset-1">
                            @foreach ($subjects as $subject)
                                <div class="checkbox">
                                    <label>
                                        <input name="subjects[]" type="checkbox" value="{{ $subject->id }}"
                                        {{ ( $teacher->subjects->where('subject_id', $subject->id)->count() > 0 ) ? 'checked' : '' }}
                                        class="form-control">
                                        {{ $subject->name }}
                                    </label>
                                </div>
                            @endforeach
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>

                    </form>
                @else
                    <ul>
                    @foreach ($teacher->subjects as $subject)
                        <li>{{ $subject->name }}</li>
                    @endforeach
                    </ul>
                @endif
            </div>
            <div class="tab-pane fade in" id="tab2">
                <h3>Biography</h3>

            </div>
        </div>
    </div>

</div>
<style>
    /* USER PROFILE PAGE */
    .card {
        margin-top: 20px;
        padding: 30px;
        background-color: rgba(214, 224, 226, 0.2);
        -webkit-border-top-left-radius:5px;
        -moz-border-top-left-radius:5px;
        border-top-left-radius:5px;
        -webkit-border-top-right-radius:5px;
        -moz-border-top-right-radius:5px;
        border-top-right-radius:5px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    .card.hovercard {
        position: relative;
        padding-top: 0;
        overflow: hidden;
        text-align: center;
        background-color: #fff;
        background-color: rgba(255, 255, 255, 1);
    }
    .card.hovercard .card-background {
        height: 130px;
    }
    .card-background img {
        -webkit-filter: blur(25px);
        -moz-filter: blur(25px);
        -o-filter: blur(25px);
        -ms-filter: blur(25px);
        filter: blur(25px);
        margin-left: -100px;
        margin-top: -200px;
        min-width: 130%;
    }
    .card.hovercard .useravatar {
        position: absolute;
        top: 15px;
        left: 0;
        right: 0;
    }
    .card.hovercard .useravatar img {
        width: 100px;
        height: 100px;
        max-width: 100px;
        max-height: 100px;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        border-radius: 50%;
        border: 5px solid rgba(255, 255, 255, 0.5);
    }
    .card.hovercard .card-info {
        position: absolute;
        bottom: 14px;
        left: 0;
        right: 0;
    }
    .card.hovercard .card-info .card-title {
        padding:0 5px;
        font-size: 20px;
        line-height: 1;
        color: #262626;
        background-color: rgba(255, 255, 255, 0.1);
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    .card.hovercard .card-info {
        overflow: hidden;
        font-size: 12px;
        line-height: 20px;
        color: #737373;
        text-overflow: ellipsis;
    }
    .card.hovercard .bottom {
        padding: 0 20px;
        margin-bottom: 17px;
    }
    .btn-pref .btn {
        -webkit-border-radius:0 !important;
    }

</style>
<script>
    window.onload = function() {
        $(".btn-pref .btn").click(function () {
            $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
            $(this).removeClass("btn-default").addClass("btn-primary");
        });
    };
</script>
@endsection
