<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h3 class="modal-title">@yield('header')</h3>
</div>

<div  class="modal-body">
    @yield('content')
</div>

@yield('scripts')
