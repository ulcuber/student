<div class="alert alert-{{ $alert }}">
    <p>{{ $text }}</p>
    @if (isset($messages) && count($messages) > 0)
    <ul>
        @foreach ($messages as $message)
        <li><i class="glyphicon glyphicon-{{ $icon }}"></i> {{ $message }}</li>
        @endforeach
    </ul>
    @endif
</div>
<script>
setTimeout(function () {
    document.getElementById('{{ $clearId }}').innerHTML='';
}, 3000);
</script>
